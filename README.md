# Space Shooter Myo

Small multiplayer game about space shooting using Urhosharp and Myo to control your ship !
<br/>
For more information, go to [./Documentation/Myastro.pdf](./Documentation/Myastro.pdf) (French version)
<br/>
Promotional video [here in 4k (online)](https://youtu.be/SCDE79eArrE) and [here in 720p (offline)](./Documentation/Promotional Video 720p30.mp4) !


## How to play

You can install Myastro with the Wizard [here](./Setup/Wizard/setup.exe).
<br/>
You can launch Myastro directly for [x86](./Setup/Portable/x86/Myastro.Desktop.exe) or [x64](./Setup/Portable/x64/Myastro.Desktop.exe).




## Installation

Clone the project on your drive : 

```bash

git clone https://gitlab.iut-clermont.uca.fr/mapoulain1/space-shooter-myo.git

```

You will see the following hierarchy of folders : 
 ```
 .
├───Code
│   └───SpaceMyo
│       ├───ClientStub
│       ├───Myastro.Desktop
│       ├───Myastro.Droid
│       ├───Myastro.Setup
│       ├───Myastro.UWP
│       ├───MyoLib
│       ├───MyoSharp
│       ├───Server
│       └───UrhoApplication
├───Documentation
│   ├───Diagram
│   └───Sketch
└───Setup


 
 ```


Open SpaceMyo.sln in Visual Studio, you will need the following extension on the Visual Studio Installer : 

* Mobile development with .NET 
* Universal Windows Platform development 
* .NET desktop development 
* ASP.NET and web development (for Server)

You will need to restore all nuget packages to build the project. 




## Usage

Launch Myastro.Desktop for the PC version of the game, Myastro.UWP will also work but there is currently no way for windows UWP applications to detect gamepad in Urhosharp so you **won't be able** to play with with a gamepad.

Launch Myastro.Droid for the Android version of the game.


MyoLib is the project that handle the connection with the myo on PC, MyoSharp is for android.

Server and Client are currently under development, but will be available soon.




## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.



## Thanks 

Huge thanks to Nicolas RAYMOND, our teacher how helped us to develop Myastro.

Thanks to Marc CHEVALDONNÉ, our other teacher how develop MyoLib and MyoSharp.

We'd like to thanks the IUT Informatique for the Myos and Gamepads.

Thanks to Kenney for all the assets, he did an incredible job, checkout [kenney.nl](https://www.kenney.nl)




## Authors
Lucie SOANEN
Maxime POULAIN

UCA - IUT Informatique - Clermont-Ferrand



## License

MIT License

Copyright (c) 2020 Myastro

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

[Project summary](https://docs.google.com/document/d/102u_sNT9x4yXYfov8p-oDBXcLnkFMt4Fhu8paH5gMik/edit?usp=sharing)<br />




### Contexte : 

L’an 3050, l’univers s’est divisé en deux camps rivaux. Depuis 100 ans les relations entre PlayerOne et PlayerTwo ne cessent de se dégrader, aujourd’hui la guerre est déclarée. Incarnez le commandant Elfo le meilleur spationaute de PlayerOne, à l’aide de votre myo, dirigez votre vaisseau et évitez les météorites. Mais vous n’êtes pas seul dans l’univers face à vous se dresse l’amiral Lucy astronaute de la garde de PlayerTwo, piloté par votre meilleur ami. Dans un combat spatial des plus féroce un seul de vous survivra aux attaques lasers ennemis. Qui de vous deux gagnera la guerre?
Votre rôle en tant que pilote sera donc le suivant : chacun des deux joueurs conduit son vaisseau grâce à son myo respectif.


### Fonctionnalités :

Le joueur pourra :
* Diriger le vaisseaux (droite/gauche)
* Diriger le vaisseaux (avant/arrière)
* Tirer des lasers
* Activer/Désactiver la musique
* Activer/Désactiver le son

L’application devra :
* Faire apparaître des obstacles (météorites)
* Générer un environnement de jeu pour 2 joueurs
* Afficher le menu principal
* Afficher un écran de fin
* Gérer les collisions

Technologies utilisées :

Moteur de jeu : 
* UrhoSharp

Matériel : 
* Myo (+ Manette XBox)

Launcher : 
* UWP
* Android Xamarin


A FAIRE POUR LA FIN DE PROJET : 

- Démonstration
- Readme complet (s'inspirer des vrais projets gitlab)
- Qualité de code
- Périphérique originaux
- Fonctionnalité originale




