﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


namespace Myastro.UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Loaded += RunGame;
        }

        private void RunGame(object sender, RoutedEventArgs e)
        {
            UrhoSurface.Run<UrhoApplication.Myastro>(new Urho.ApplicationOptions("Data") { Width = (int)UrhoSurface.ActualWidth, Height = (int)UrhoSurface.ActualHeight, LimitFps = true });

        }
    }
}
