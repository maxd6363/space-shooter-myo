﻿using System;

namespace UrhoApplication.Utils
{
    public class UnknownTypeException : Exception
    {
        public UnknownTypeException(string message = "") : base(message)
        {
        }
    }
}
