﻿using System;
using System.Globalization;
using Urho;
using Urho.Resources;

using Urho.Gui;
using Urho.IO;

namespace UrhoApplication.Utils
{
    public class KeyboardShortcutHandler : Component
    {

        public KeyboardShortcutHandler()
        {
            Myastro.Current.Scene.CreateChild().AddComponent(this);
            Application.Current.Update += Update;
        }

        private void Update(UpdateEventArgs obj)
        {
            if (ToolBox.IsKeyDown(AppConfig.Current.ShortcutDrawDebug, false))
            {
                AppConfig.Current.DrawDebug = !AppConfig.Current.DrawDebug;
            }
            
            if (ToolBox.IsKeyDown(AppConfig.Current.ShortcutFPS, false))
            {
                Myastro.Current.GUIFps.Toggle();
            }

            if (ToolBox.IsKeyDown(AppConfig.Current.ShortcutFullscreen, false))
            {
                Myastro.Current.Graphics.ToggleFullscreen();
            }

            if (ToolBox.IsKeyDown(AppConfig.Current.ShortcutScreenshot, false))
            {
                Image screenshot = new Image();
                Application.Current.Graphics.TakeScreenShot(screenshot);
                screenshot.SavePNG(new FileSystem().ProgramDir + $"Screenshot_{Myastro.Current.GetType()}_{DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss", CultureInfo.InvariantCulture)}.png");
            }



            if (ToolBox.IsKeyDown(AppConfig.Current.ShortcutCameraBackward, true))
            {
                Myastro.Current.Camera.Zoom -= 0.001f;
            }

            if (ToolBox.IsKeyDown(AppConfig.Current.ShortcutCameraForward, true))
            {
                Myastro.Current.Camera.Zoom += 0.001f;
            }



        }




    }
}
