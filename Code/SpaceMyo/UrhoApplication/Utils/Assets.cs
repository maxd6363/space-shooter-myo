﻿namespace UrhoApplication.Utils
{
    public class Assets
    {
        public static class Materials
        {
            public const string BackgroundBlack = "Materials/background_black.xml";
            public const string BackgroundBlue = "Materials/background_blue.xml";
            public const string BackgroundDarkPurple = "Materials/background_darkPurple.xml";
            public const string BackgroundPurple = "Materials/background_purple.xml";
        }

        public static class Models
        {
            public const string Sphere = "Models/sphere.mdl";
            public const string Box = "Models/box.mdl";
        }

        public static class Sounds
        {
            public const string Laser1 = "Sounds/sfx_laser1.ogg";
            public const string Laser2 = "Sounds/sfx_laser2.ogg";
            public const string Lose = "Sounds/sfx_lose.ogg";
            public const string ShieldDown = "Sounds/sfx_shieldDown.ogg";
            public const string ShieldUp = "Sounds/sfx_shieldUp.ogg";
            public const string TwoTone = "Sounds/sfx_twoTone.ogg";
            public const string Zap = "Sounds/sfx_zap.ogg";

            public static class Musics
            {
                public const string Music1 = "Sounds/Musics/music1.ogg";
                public const string Music2 = "Sounds/Musics/music2.ogg";
                public const string Music3 = "Sounds/Musics/music3.ogg";
                public const string Music4 = "Sounds/Musics/music4.ogg";
                public const string Music5 = "Sounds/Musics/music5.ogg";

            }

        }

        public static class Fonts
        {
            public const string AnonymousPro = "Fonts/anonymous_pro.ttf";
            public const string TimeNewRoman = "Fonts/time_new_roman.ttf";
            public const string KenvectorFuture = "Fonts/kenvector_future.ttf";
        }

        public static class Textures
        {
            public const string ButtonBlue = "Textures/button_blue.png";
            public const string ButtonGreen = "Textures/button_green.png";
            public const string ButtonRed = "Textures/button_red.png";
            public const string ButtonYellow = "Textures/button_yellow.png";
            public const string SmallButtonBlue = "Textures/small_button_blue.png";
            public const string IconInputNothing = "Textures/input_nothing.png";
            public const string IconInputMyo = "Textures/input_myo.png";
            public const string IconInputGamepad = "Textures/input_gamepad.png";
            public const string IconInputGamepadBeta = "Textures/input_gamepad_beta.png";
            public const string IconInputKeyboard = "Textures/input_keyboard.png";
            public const string IconInputMouse = "Textures/input_mouse.png";
            public const string Cursor = "Textures/cursor.png";



        }

        public static class PostProcess
        {
            public const string AutoExposure = "PostProcess/auto_exposure.xml";
            public const string Bloom = "PostProcess/bloom.xml";
            public const string BloomHDR = "PostProcess/bloom_hdr.xml";
            public const string Blur = "PostProcess/blur.xml";
            public const string ColorCorrection = "PostProcess/color_correction.xml";
            public const string FXAA2 = "PostProcess/fxaa2.xml";
            public const string FXAA3 = "PostProcess/fxaa3.xml";
            public const string GammaCorrection = "PostProcess/gamma_correction.xml";
            public const string GreyScale = "PostProcess/grey_scale.xml";
            public const string Tonemap = "PostProcess/tonemap.xml";

        }

        public static class Sprites
        {
            public static class Backgrounds
            {
                public const string BackgroundBlack = "Sprites/Background/background_black.png";
                public const string BackgroundBlue = "Sprites/Background/background_blue.png";
                public const string BackgroundDarkPurple = "Sprites/Background/background_darkPurple.png";
                public const string BackgroundPurple = "Sprites/Background/background_purple.png";


            }


            public static class Ships
            {
                public const string Ship1Blue = "Sprites/Ship/player_ship1_blue.png";
                public const string Ship1Red = "Sprites/Ship/player_ship1_red.png";
                public const string Ship1Green = "Sprites/Ship/player_ship1_green.png";
                public const string Ship1Orange = "Sprites/Ship/player_ship1_orange.png";
            }

            public static class Flame
            {
                public const string SmallBlue = "Sprites/Flame/small_blue.png";
                public const string BigBlue = "Sprites/Flame/big_blue.png";
            }


            public static class Projectil
            {
                public static class Laser
                {
                    public const string Laser1 = "Sprites/Laser/laser1.png";

                }

            }




            public static class Asteroids
            {
                public static class Tiny
                {
                    public const string Meteor1 = "Sprites/Asteroid/meteor_brown_tiny1.png";
                    public const string Meteor2 = "Sprites/Asteroid/meteor_brown_tiny2.png";
                    public const string Meteor3 = "Sprites/Asteroid/meteor_grey_tiny1.png";
                    public const string Meteor4 = "Sprites/Asteroid/meteor_grey_tiny2.png";
                }
                public static class Small
                {
                    public const string Meteor1 = "Sprites/Asteroid/meteor_brown_small1.png";
                    public const string Meteor2 = "Sprites/Asteroid/meteor_brown_small2.png";
                    public const string Meteor3 = "Sprites/Asteroid/meteor_grey_small1.png";
                    public const string Meteor4 = "Sprites/Asteroid/meteor_grey_small2.png";
                }

                public static class Medium
                {
                    public const string Meteor1 = "Sprites/Asteroid/meteor_brown_med1.png";
                    public const string Meteor2 = "Sprites/Asteroid/meteor_brown_med2.png";
                    public const string Meteor3 = "Sprites/Asteroid/meteor_grey_med1.png";
                    public const string Meteor4 = "Sprites/Asteroid/meteor_grey_med2.png";
                }

                public static class Big
                {
                    public const string Meteor1 = "Sprites/Asteroid/meteor_brown_big1.png";
                    public const string Meteor2 = "Sprites/Asteroid/meteor_brown_big2.png";
                    public const string Meteor3 = "Sprites/Asteroid/meteor_brown_big3.png";
                    public const string Meteor4 = "Sprites/Asteroid/meteor_brown_big4.png";
                    public const string Meteor5 = "Sprites/Asteroid/meteor_grey_big1.png";
                    public const string Meteor6 = "Sprites/Asteroid/meteor_grey_big2.png";
                    public const string Meteor7 = "Sprites/Asteroid/meteor_grey_big3.png";
                    public const string Meteor8 = "Sprites/Asteroid/meteor_grey_big4.png";
                }

            }



            public static class Bonus
            {
                public static class Shield
                {
                    public const string Bronze = "Sprites/Bonus/Shield/bronze.png";
                    public const string Silver = "Sprites/Bonus/Shield/silver.png";
                    public const string Gold = "Sprites/Bonus/Shield/gold.png";
                    public const string Effect1 = "Sprites/Bonus/Shield/shield_effect_1.png";
                    public const string Effect2 = "Sprites/Bonus/Shield/shield_effect_2.png";
                    public const string Effect3 = "Sprites/Bonus/Shield/shield_effect_3.png";
                }
                public static class Power
                {
                    public const string Bronze = "Sprites/Bonus/Power/bronze.png";
                    public const string Silver = "Sprites/Bonus/Power/silver.png";
                    public const string Gold = "Sprites/Bonus/Power/gold.png";
                }

                public static class Pill
                {
                    public const string Blue = "Sprites/Bonus/Pill/blue.png";
                    public const string Green = "Sprites/Bonus/Pill/green.png";
                    public const string Red = "Sprites/Bonus/Pill/red.png";
                    public const string Yellow = "Sprites/Bonus/Pill/yellow.png";
                }
            }


        }

        public static class Particles
        {

            public const string Fire = "Particules/fire.xml";
            public const string Sun = "Particules/sun.pex";

        }


    }
}


