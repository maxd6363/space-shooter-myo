﻿using System.Collections.Generic;

namespace UrhoApplication.Utils.I18n
{
    public static class StringValues
    {
        public static Dictionary<string, string> EN_EN { get; }
        public static Dictionary<string, string> FR_FR { get; }

        static StringValues()
        {
            EN_EN = new Dictionary<string, string>
            {
                { "SINGLEPLAYER", "SINGLEPLAYER" },
                { "MULTIPLAYER", "MULTIPLAYER" },
                { "SETTINGS", "SETTINGS" },
                { "BACK", "BACK" },
                { "PLAYER1", "PLAYER 1" },
                { "PLAYER2", "PLAYER 2" },
                { "PLAYER3", "PLAYER 3" },
                { "PLAYER4", "PLAYER 4" },
                { "START_MULTIPLAYER", "START" },
                { "START_SINGLEPLAYER", "START" },
                { "PLAYER", "PLAYER" },
                { "SYNCHRONIZE_MYO", "MYO" }
            };

            FR_FR = new Dictionary<string, string>
            {
                { "SINGLEPLAYER", "HISTOIRE" },
                { "MULTIPLAYER", "MULTIJOUEUR" },
                { "SETTINGS", "PARAMETRES" },
                { "BACK", "RETOUR" },
                { "PLAYER1", "JOUEUR 1" },
                { "PLAYER2", "JOUEUR 2" },
                { "PLAYER3", "JOUEUR 3" },
                { "PLAYER4", "JOUEUR 4" },
                { "START_MULTIPLAYER", "JOUER" },   
                { "START_SINGLEPLAYER", "JOUER" },
                { "PLAYER", "PLAYER" },
                { "SYNCHRONIZE_MYO", "MYO" }
            };




        }


    }
}
