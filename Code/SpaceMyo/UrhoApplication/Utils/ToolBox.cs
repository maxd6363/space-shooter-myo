﻿using System;
using System.Diagnostics;
using Urho;
using UrhoApplication.UIElement;

namespace UrhoApplication.Utils
{
    public static class ToolBox
    {

        public static Random Random { get; } = new Random();


        public static float RandomFloat(float min, float max)
        {
            return (float)(min + (max - min) * Random.NextDouble());
        }

        public static float RandomFloat(float max)
        {
            return RandomFloat(0, max);
        }

        public static float RandomFloat(double min, double max)
        {
            return RandomFloat((float)min, (float)max);
        }

        public static float RandomFloat(double max)
        {
            return RandomFloat(0f, (float)max);
        }

        public static bool IsKeyDown(Key key, bool continuous)
        {
            return continuous ? Application.Current.Input.GetKeyDown(key) : Application.Current.Input.GetKeyPress(key);

        }

        public static Vector2 DegreeToVectorNomalized(float degree)
        {
            return new Vector2((float)DCos(degree), (float)DSin(degree));
        }


        public static double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        public static double RadianToDegree(double angle)
        {
            return angle * 180.0 / Math.PI;
        }


        public static double DCos(double angle)
        {
            return Math.Cos(DegreeToRadian(angle));
        }
        public static double DSin(double angle)
        {
            return Math.Sin(DegreeToRadian(angle));
        }

        public static IntVector2 AdaptedButtonSize(float percentW, ButtonSize buttonSize = ButtonSize.LARGE)
        {
            float _ratio;

            switch (buttonSize)
            {
            
                case ButtonSize.SQUARE:
                    _ratio = (float)AppConfig.Current.UISmallButtonRatio.Y / AppConfig.Current.UISmallButtonRatio.X;
                    break;

                default:
                    _ratio = (float)AppConfig.Current.UIButtonRatio.Y / AppConfig.Current.UIButtonRatio.X;
                    break;

            }
            return new IntVector2((int)(Application.Current.Graphics.Width * percentW), (int)(Application.Current.Graphics.Width * percentW * _ratio));
        }


        public static IntVector2 WindowSize()
        {
            return new IntVector2(Application.Current.Graphics.Width, Application.Current.Graphics.Height);
        }

        public static IntVector2 RelativePosition(float percentW, float percentH)
        {
            return new IntVector2((int)(Application.Current.Graphics.Width * percentW), (int)(Application.Current.Graphics.Width * percentH));
        }


        public static bool IsPosInRect(int x, int y, IntRect rect)
        {
            return x >= rect.Left && x <= rect.Right && y >= rect.Top && y <= rect.Bottom;
        }

        public static int RelativeFontSize(int size)
        {
            return (int)(Application.Current.Graphics.Width / 1500f * size);
        }
        
        public static IntVector2 RelativeIconSize()
        {
            return new IntVector2((int)(Application.Current.Graphics.Width / 20f) , (int)(Application.Current.Graphics.Width / 20f));
        }


        public static decimal Map(decimal value, decimal fromSource, decimal toSource, decimal fromTarget, decimal toTarget)
        {
            return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
        }

    }
}
