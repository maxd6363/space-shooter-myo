﻿using System.Collections.Generic;
using Urho;
using UrhoApplication.Input;

namespace UrhoApplication.Utils
{
    public class AppConfig
    {

        private static AppConfig _current = null;
        public static AppConfig Current
        {
            get
            {
                if (_current == null)
                    _current = new AppConfig();
                return _current;
            }
        }

        private AppConfig() { }

        public IList<string> PlayerNames { get; } = new List<string>() { "Elfo", "Lucy", "Bean", "Chazz" };
        public IList<Color> DefaultPlayerColor { get; } = new List<Color>() { Color.Cyan, Color.Red, Color.Green, Color.Yellow };

        public int CameraZ { get => -4; }
        public float CameraRumbleCounter { get => 0.4f; }
        public float CameraRumbleScale { get => 0.4f; }
        public int BackgroundZ { get => 10; }
        public int BackgroundScale { get => 500; }



        public ICollection<string> PostProcessing { get; } = new List<string>()
        {
            Assets.PostProcess.Bloom,
            Assets.PostProcess.FXAA3,
            Assets.PostProcess.ColorCorrection,
            Assets.PostProcess.GammaCorrection
        };


        public bool DrawDebug { get; set; } = false;


        public Dictionary<string, string> Language { get; set; }



        public string DefaultBackground { get => Assets.Materials.BackgroundPurple; }
        public int DefaultBackgroundUVRepeat { get => 100; }





        public int DefaultFPSFontSize { get => 20; }
        public int DefaultButtonFontSize { get => 40; }
        public float DefaultButtonWidth { get => 0.4f; }
        public int DefaultFPSMargin { get => 20; }
        public Color DefaultFPSColor { get => Color.White; }
        public Color DefaultButtonColor { get => Color.White; }


        public int DefaultTagFontSize { get => 220; }
        public Vector2 DefaultTagMargin { get => new Vector2(0, -6); }
        public float DefaultTagScaleRatio { get => 3; }






        // (0, 0) is in the middle of the screen
        public int EndOfMap_PlusX { get => 40; }
        public int EndOfMap_PlusY { get => (int)((float)Application.Current.Graphics.Height / Application.Current.Graphics.Width * EndOfMap_PlusX); }
        public int EndOfMap_MinusX { get => -EndOfMap_PlusX; }
        public int EndOfMap_MinusY { get => -EndOfMap_PlusY; }



        public Key ShortcutScreenshot { get => Key.F2; }
        public Key ShortcutFPS { get => Key.F3; }
        public Key ShortcutDrawDebug { get => Key.F8; }
        public Key ShortcutFullscreen { get => Key.F11; }
        public Key ShortcutCameraBackward { get => Key.KP_Minus; }
        public Key ShortcutCameraForward { get => Key.KP_Plus; }




        public float ShipThrusterBaseSpeed { get => 20f; }
        public float ShipThrusterReverseSpeed { get => -ShipThrusterBaseSpeed / 2f; }
        public float ShipThrusterBoostedSpeed { get => ShipThrusterBaseSpeed * 3f; }
        public float ShipThrusterZeroSpeed { get => 0f; }
        public float ShipAngularVelocityMargin { get => 0.0f; }
        public float ShipAngularVelocityReset { get => 0.05f; }
        public float ShipLinearVelocityRatio { get => 100f; }
        public float ShipShapeRadius { get => 2.2f; }
        public float ShipShapeAuxMarginX { get => 2.5f; }
        public float ShipShapeAuxMarginY { get => -0.9f; }
        public Vector2 ShipShapeAuxSize { get => new Vector2(1.5f, 5f); }
        public float ShipShapeMainMarginY { get => -0.3f; }
        public float ShipShapDensity { get => 0.3f; }
        public float ShipDurability { get => 500f; }
        public float ShipDeadOpacity { get => 0.2f; }
        public float ShipDeadDurationAnimation { get => 20f; }
        public float ShipDefaultFiringThrottling { get => 0.07f; }
        public float ShipSpawnAnimationTiming { get => 0.2f; }
        public float ShipMaxLinearVelocity { get => 6.0f; }
        

               
        public float LaserScaleRatio { get => 0.6f; }
        public Vector2 LaserShapeSize { get => new Vector2(1f, 4.1f); }
        public float LaserShapeDensity { get => 0.01f; }
        public float LaserShapeImpulse { get => 0.06f; }
        public Vector3 LaserPositionDelta { get => new Vector3(0f, 1.5f, 0); }
        public int LaserDispawnPlusX { get => EndOfMap_PlusX * 20; }
        public int LaserDispawnPlusY { get => EndOfMap_PlusY * 20; }
        public int LaserDispawnMinusX { get => EndOfMap_MinusX * 20; }
        public int LaserDispawnMinusY { get => EndOfMap_MinusY * 20; }
        public float LaserDamageInflicted { get => 30f; }


        
        public float EntityRotationLeftSpeed { get => 0.09f; }
        public float EntityRotationRightSpeed { get => -EntityRotationLeftSpeed; }
        public float EntityRotationNoSpeed { get => 0f; }
        public float EntityInitialInputForce { get => 0.5f; }
        public float EntityMaxAngularVelocity { get => 2f; }
        public float EntityDurability { get => 100f; }
        public float EntitySpawnAnimationTiming { get => 1.0f; }



        public float ShieldShapDensity { get => 0.02f; }
        public float ShieldProtection { get => 300f; }
        public float ShieldShapeSize { get => 5f; }


        public float HealEffectDurabilityAdded { get => 50f; }


        /// <summary>
        /// time in second
        /// </summary>
        public float BonusIconDurability { get => 8f; }


        /// <summary>
        /// time in second
        /// </summary>
        public float BonusDurabilityBronze { get => 3f; }


        /// <summary>
        /// time in second
        /// </summary>
        public float BonusDurabilitySilver { get => 6f; }


        /// <summary>
        /// time in second
        /// </summary>
        public float BonusDurabilityGold { get => 9f; }



        public float BonusTickRate { get => 5f; }
        public int BonusMaxCount{ get => 10; }




        public float AsteroidScaleByDamage { get => 0.95f; }
        public float AsteroidScaleByTime { get => 0.5f; }
        public float AsteroidScaleToTime { get => 4f; }


        public float FlameRumbleRatio { get => 0.2f; }



        public float GamepadMarginAnalogic { get; set; } = 0.05f;
        public ImplementedInputs DefaultInputSelected { get; set; } = ImplementedInputs.UWP_GAMEPAD_URHO;


        public float MyoMarginAnalogic { get; set; } = 0.5f;


        public int NumberOfAsteroidsAverage { get; set; } = 60;



        public IntVector2 UIButtonRatio { get => new IntVector2(1080, 260); }
        public float UIBackButtonMargin { get => -0.01f; }
        public float UIBackButtonSize { get => 0.14f; }
        public float UISynchronMyoButtonMargin { get => 0.01f; }
        public float UISynchronMyoButtonSize { get => 0.14f; }
        public IntVector2 UISmallButtonRatio { get => new IntVector2(260, 260); }
        public IntVector2 UIButtonMaxSize { get => new IntVector2((int)(ToolBox.WindowSize().X * 0.8f), (int)(ToolBox.WindowSize().Y * 0.8f)); }
        public IntVector2 UIIconPosition { get => new IntVector2(10, 0); }






        public int PlayerAddScore { get => 20; }








        // MainMenu
        public string IDSingleplayer { get => "SINGLEPLAYER"; }
        public string IDMultiplayer { get => "MULTIPLAYER"; }
        public string IDSettings { get => "SETTINGS"; }


        // SettingsMenu
        public string IDBackFromSettings { get => "BACK"; }
        public string IDSynchronMyoFromSettings { get => "SYNCHRONIZE_MYO"; }


        // SingleplayerMenu
        public IList<string> IDLevels { get; } = new List<string>()
        {
            "1", "2", "3" ,"4", "5", "6", "7", "8", "9"
        };
        public string IDBackFromSingleplayer { get => "BACK"; }
        public string IDStartSingleplayer { get => "START_SINGLEPLAYER"; }
        public string IDPlayer { get => "PLAYER"; }




        // MultiplayerMenu
        public string IDPlayer1 { get => "PLAYER1"; }
        public string IDPlayer2 { get => "PLAYER2"; }
        public string IDPlayer3 { get => "PLAYER3"; }
        public string IDPlayer4 { get => "PLAYER4"; }
        public string IDStartMultiplayer { get => "START_MULTIPLAYER"; }
        public string IDBackFromMultiplayerMenu { get => "BACK"; }




        // MultiplayerGameMenu
        public string IDBackFromMultiplayerGame { get => "BACK"; }



        // SingleplayerGameMenu
        public string IDBackFromSingleplayerGame { get => "BACK"; }











    }
}
