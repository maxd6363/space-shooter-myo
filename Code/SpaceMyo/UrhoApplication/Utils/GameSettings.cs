﻿using System;
using System.Collections.Generic;
using UrhoApplication.Input;

namespace UrhoApplication.Utils
{
    public class GameSettings
    {
        public IList<ImplementedInputs> MultiplayerInputs { get; }

        public ImplementedInputs SingleplayerInput { get; protected set; }

        public float MusicGain
        {
            get => _musicGain;
            set
            {
                if (value != _musicGain)
                {
                    _musicGain = value;
                    MusicGainChanged?.Invoke(_musicGain);
                }
            }
        }
        public float SoundGain
        {
            get => _soundGain;
            set
            {
                if (value != _soundGain)
                {
                    _soundGain = value;
                    MusicGainChanged?.Invoke(_soundGain);
                }
            }
        }


        public event Action<float> MusicGainChanged;
        public event Action<float> SoundGainChanged;

        private float _soundGain;
        private float _musicGain;


        public GameSettings()
        {
            MultiplayerInputs = new List<ImplementedInputs>();

        }

        public void SetSingleplayerInput(ImplementedInputs selectedInput = ImplementedInputs.UWP_KEYBOARD)
        {
            SingleplayerInput = selectedInput;
        }


        public void AddMultiplayerInput(ImplementedInputs selectedInput = ImplementedInputs.UWP_KEYBOARD)
        {
            if (selectedInput == ImplementedInputs.NONE) return;
            MultiplayerInputs.Add(selectedInput);
        }
        public void ResetMultiplayerInput()
        {
            MultiplayerInputs.Clear();
        }

    }
}
