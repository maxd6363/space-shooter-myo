﻿using System;
using System.Reflection;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Asteroid
{
    class SimpleAsteroidSmall : SizedAsteroid
    {
        [Obsolete("Please use the Factory (UrhoApplication.Factory.AbstractFactory) to instanciate an asteroid")]
        public SimpleAsteroidSmall() : base()
        {
            string _spriteToLoad;

            // Permet de récupérer tous les attributs d'une classe, pour ensuite en séléctionner un au hasard, pour diversifier la population d'asteroid 
            FieldInfo[] _attributs;

            Node.Scale *= 0.35f;
            Body.Mass = 20;
            (Shape as CollisionCircle2D).Radius = 1.2f;
            
            Durability *= 0.2f;

            _attributs = typeof(Assets.Sprites.Asteroids.Small).GetFields();
            _spriteToLoad = (string)_attributs[ToolBox.Random.Next(_attributs.Length)].GetValue(null);
            Sprite.Sprite = Application.ResourceCache.GetSprite2D(_spriteToLoad);
            DoSpawnAnimation();
        }
    }
}
