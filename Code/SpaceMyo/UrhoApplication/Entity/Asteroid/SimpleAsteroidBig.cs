﻿using System;
using System.Diagnostics;
using System.Reflection;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Asteroid
{
    public class SimpleAsteroidBig : SizedAsteroid
    {
        [Obsolete("Please use the Factory (UrhoApplication.Factory.AbstractFactory) to instanciate an asteroid")]
        public SimpleAsteroidBig()
        {
            string _spriteToLoad;

            // Permet de récupérer tous les attributs d'une classe, pour ensuite en séléctionner un au hasard, pour diversifier la population d'asteroid 
            FieldInfo[] _attributs;

            Node.Scale *= 0.8f;
            Body.Mass = 100;
            (Shape as CollisionCircle2D).Radius = 3.1f;

            Durability *= 2;

            _attributs = typeof(Assets.Sprites.Asteroids.Big).GetFields();
            _spriteToLoad = (string)_attributs[ToolBox.Random.Next(_attributs.Length)].GetValue(null);
            try
            {
                Sprite.Sprite = Application.ResourceCache.GetSprite2D(_spriteToLoad);
            }
            catch (AccessViolationException e)
            {
                Debug.WriteLine(e.StackTrace);
            }
            DoSpawnAnimation();
        }
    }
}
