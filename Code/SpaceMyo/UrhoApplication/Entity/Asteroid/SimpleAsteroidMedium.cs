﻿using System;
using System.Reflection;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Asteroid
{
    public class SimpleAsteroidMedium : SizedAsteroid
    {
        [Obsolete("Please use the Factory (UrhoApplication.Factory.AbstractFactory) to instanciate an asteroid")]
        public SimpleAsteroidMedium() : base()
        {
            string _spriteToLoad;

            // Permet de récupérer tous les attributs d'une classe, pour ensuite en séléctionner un au hasard, pour diversifier la population d'asteroid 
            FieldInfo[] _attributs;

            Node.Scale *= 0.5f;
            Body.Mass = 50;
            (Shape as CollisionCircle2D).Radius = 1.6f;

            Durability *= 0.8f;

            _attributs = typeof(Assets.Sprites.Asteroids.Medium).GetFields();
            _spriteToLoad = (string)_attributs[ToolBox.Random.Next(_attributs.Length)].GetValue(null);
            Sprite.Sprite = Application.ResourceCache.GetSprite2D(_spriteToLoad);
            DoSpawnAnimation();

        }
    }
}
