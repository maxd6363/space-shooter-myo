﻿using Urho;
using Urho.Urho2D;
using UrhoApplication.EntityGraphic;

namespace UrhoApplication.Entity.Asteroid
{
    public abstract class Asteroid : Entity
    {
        protected SoundCustom Sound { get; }

        public Asteroid() : base()
        {
            Shape = Node.CreateComponent<CollisionCircle2D>();
            Sound = Node.CreateComponent<SoundCustom>();
        }


        public override void Update(UpdateEventArgs obj)
        {
            base.Update(obj);
        }

    }
}
