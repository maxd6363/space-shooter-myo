﻿using System;
using Urho;
using Urho.Actions;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Asteroid
{
    public abstract class SizedAsteroid : Asteroid
    {



     
        [Obsolete("Please use the Factory (UrhoApplication.Factory.AbstractFactory) to instanciate an asteroid")]
        public SizedAsteroid() : base()
        {
            RandomRotation();
            RandomPosition();
            PushImpulse();
        }


        public override void Update(UpdateEventArgs obj)
        {
            base.Update(obj);
        }

        public override async void TakeDamage(float damage)
        {
            if (Durability == 0) return;
            Sound.Play(Application.Current.ResourceCache.GetSound(Assets.Sounds.Zap));
            Durability -= damage;

            await Node.RunActionsAsync(new EaseIn(new ScaleBy(AppConfig.Current.AsteroidScaleByTime, AppConfig.Current.AsteroidScaleByDamage), 1));

            if (Durability <= 0)
            {
                Durability = 0;
                await Node.RunActionsAsync(new EaseIn(new ScaleTo(AppConfig.Current.AsteroidScaleToTime, 0), 1));
                Delete();
            }
        }

    }
}
