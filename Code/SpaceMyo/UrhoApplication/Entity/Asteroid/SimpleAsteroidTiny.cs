﻿using System;
using System.Reflection;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Asteroid
{
    class SimpleAsteroidTiny : SizedAsteroid
    {
        [Obsolete("Please use the Factory (UrhoApplication.Factory.AbstractFactory) to instanciate an asteroid")]
        public SimpleAsteroidTiny() : base()
        {
            string _spriteToLoad;

            // Permet de récupérer tous les attributs d'une classe, pour ensuite en séléctionner un au hasard, pour diversifier la population d'asteroid 
            FieldInfo[] _attributs;

            Node.Scale *= 0.2f;
            Body.Mass = 10;
            (Shape as CollisionCircle2D).Radius = 0.8f;

            Durability *= 0.05f;

            _attributs = typeof(Assets.Sprites.Asteroids.Tiny).GetFields();
            _spriteToLoad = (string)_attributs[ToolBox.Random.Next(_attributs.Length)].GetValue(null);
            Sprite.Sprite = Application.ResourceCache.GetSprite2D(_spriteToLoad);
            DoSpawnAnimation();
        }
    }
}
