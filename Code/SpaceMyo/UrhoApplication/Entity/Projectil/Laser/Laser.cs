﻿using System;
using System.Diagnostics;
using Urho;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Projectil.Laser
{
    public abstract class Laser : Projectil
    {
        public float DamageInflicted { get; protected set; }
        public Entity AttachedTo { get; }
        protected bool Dispawn { get; }

        public Laser(Entity attachedTo, bool dispawn = true ) : base()
        {
            Dispawn = dispawn;
            DamageInflicted = AppConfig.Current.LaserDamageInflicted;
            AttachedTo = attachedTo;
            AttachedTo.Node.CreateChild("Laser").AddComponent(this);


            Sprite = Node.CreateComponent<StaticSprite2D>();
            Body = Node.CreateComponent<RigidBody2D>();
            Body.BodyType = BodyType2D.Dynamic;

            Shape = Node.CreateComponent<CollisionBox2D>();
            (Shape as CollisionBox2D).Size = AppConfig.Current.LaserShapeSize;
            (Shape as CollisionBox2D).Density = AppConfig.Current.LaserShapeDensity;

            Node.Position = AppConfig.Current.LaserPositionDelta;



        }

        public override void Update(UpdateEventArgs obj)
        {
            try
            {
                if (Deleted) return;
                base.Update(obj);
                if (Dispawn)
                {
                    DispawnWhenOutOfBounds();
                }

            }
            catch (InvalidOperationException)
            {

            }

        }

        private void DispawnWhenOutOfBounds()
        {
            if (Node.Position.X < AppConfig.Current.LaserDispawnMinusX || Node.Position.X > AppConfig.Current.LaserDispawnPlusX || Node.Position.Y < AppConfig.Current.LaserDispawnMinusY || Node.Position.Y > AppConfig.Current.LaserDispawnPlusY)
                Delete();
        }

        public override void Delete()
        {
            base.Delete();
            try
            {
                AttachedTo.Node.RemoveChild(Node);
            }
            catch (NullReferenceException)
            {
                Debug.WriteLine("Cannot delete laser from Scene, a collision must have occured between the laser and his ship at throwing moment");
            }
        }


    }
}
