﻿using Urho;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Projectil.Laser
{
    public class SimpleLaser : Laser
    {
        public SimpleLaser(Entity attachedTo,  bool dispawn = true) : base(attachedTo, dispawn)
        {
            Sprite.Sprite = Application.ResourceCache.GetSprite2D(Assets.Sprites.Projectil.Laser.Laser1);
            Node.Scale *= AppConfig.Current.LaserScaleRatio;
            Body.ApplyLinearImpulseToCenter(ToolBox.DegreeToVectorNomalized(AttachedTo.Node.Rotation2D + 90) * AppConfig.Current.LaserShapeImpulse, true);

        }

    }
}
