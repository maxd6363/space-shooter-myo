﻿using Urho;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Projectil
{
    public abstract class Projectil : Entity
    {
        public Projectil() : base(false, false)
        {

        }

        public override void Update(UpdateEventArgs obj)
        {
            base.Update(obj);
        }
    }
}
