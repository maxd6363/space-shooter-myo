﻿namespace UrhoApplication.Entity
{
    public interface IDeletable
    {
        void Delete();
    }
}
