﻿namespace UrhoApplication.Entity
{
    public interface IDamagable
    {
        void TakeDamage(float damage);


    }
}
