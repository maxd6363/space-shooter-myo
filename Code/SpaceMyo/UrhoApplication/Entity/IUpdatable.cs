﻿using Urho;

namespace UrhoApplication.Entity
{
    public interface IUpdatable
    {
        void Update(UpdateEventArgs obj);

    }
}
