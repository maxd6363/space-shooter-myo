﻿using System;
using System.Diagnostics;
using Urho;
using Urho.Actions;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity
{
    public abstract class Entity : Component, IDamagable, IDeletable, IUpdatable
    {

        public StaticSprite2D Sprite { get; protected set; }
        public RigidBody2D Body { get; protected set; }
        public CollisionShape2D Shape { get; protected set; }
        public bool Wrapping { get; set; }
        public float Durability { get; set; }
        public bool Deleted { get; protected set; } = false;
        public bool AbleToTakeDamage { get; set; } = true;

        public event Action<Entity> EntityWasDeleted
        {
            add => _entityWasDeleted += value;
            remove => _entityWasDeleted -= value;
        }
        protected Action<Entity> _entityWasDeleted;




        public Entity(bool wrapping = true, bool addToScene = true) : base()
        {
            
            if (addToScene)
            {
                Myastro.Current.Scene.CreateChild("Entity").AddComponent(this);

                Sprite = Node.CreateComponent<StaticSprite2D>();
                Body = Node.CreateComponent<RigidBody2D>();
                Body.BodyType = BodyType2D.Dynamic;
                Node.Position = Vector3.Zero;
                



            }
            Wrapping = wrapping;
            Durability = AppConfig.Current.EntityDurability;

            ReceiveSceneUpdates = true;
            Application.Current.Update += Update;
        }


        protected virtual async void DoSpawnAnimation()
        {
            var _nodeScale = Node.Scale;
            Node.Scale = Vector3.Zero;
            await Node.RunActionsAsync(new EaseIn(new ScaleTo(AppConfig.Current.EntitySpawnAnimationTiming, _nodeScale.X, _nodeScale.Y, _nodeScale.Y), 1));
        }

        public virtual void RandomPosition()
        {
            Node.Position = new Vector3(ToolBox.Random.Next(AppConfig.Current.EndOfMap_MinusX, AppConfig.Current.EndOfMap_PlusX), ToolBox.Random.Next(AppConfig.Current.EndOfMap_MinusY, AppConfig.Current.EndOfMap_PlusY), 0);
        }

        public virtual void RandomRotation()
        {
            Node.Rotation2D = (float)ToolBox.Random.NextDouble() * 180f;
        }

        public virtual void PushImpulse()
        {
            Body.ApplyLinearImpulseToCenter(new Vector2(ToolBox.RandomFloat(-AppConfig.Current.EntityInitialInputForce, AppConfig.Current.EntityInitialInputForce), ToolBox.RandomFloat(-AppConfig.Current.EntityInitialInputForce, AppConfig.Current.EntityInitialInputForce)), true);
        }


        public virtual void Update(UpdateEventArgs obj)
        {
            if (Deleted) return;
            base.OnUpdate(obj.TimeStep);
            if (Wrapping)
            {
                WrapEntityOnScreen();
            }

            LimitRotation();
        }


        private void LimitRotation()
        {

            try
            {
                if (Body == null) return;

                if (Body.AngularVelocity > AppConfig.Current.EntityMaxAngularVelocity)
                    Body.AngularVelocity = AppConfig.Current.EntityMaxAngularVelocity;
                if (Body.AngularVelocity < -AppConfig.Current.EntityMaxAngularVelocity)
                    Body.AngularVelocity = -AppConfig.Current.EntityMaxAngularVelocity;

            }
            catch (InvalidOperationException) { }
            //catch (NullReferenceException) { }
        }




        private async void WrapEntityOnScreen()
        {
            if (Node.Position.X < AppConfig.Current.EndOfMap_MinusX * 1.05f || Node.Position.X > AppConfig.Current.EndOfMap_PlusX * 1.05f)
            {
                Node.Position = new Vector3(-Node.Position.X * 0.95f, Node.Position.Y, Node.Position.Z);
                await Node.RunActionsAsync(new EaseIn(new FadeIn(1), 1));
            }


            if (Node.Position.Y < AppConfig.Current.EndOfMap_MinusY * 1.05f || Node.Position.Y > AppConfig.Current.EndOfMap_PlusY * 1.05f)
            {
                Node.Position = new Vector3(Node.Position.X, -Node.Position.Y * 0.95f, Node.Position.Z);
                await Node.RunActionsAsync(new EaseIn(new FadeIn(1), 1));
            }
        }

        public virtual void TakeDamage(float damage)
        {
            if (!AbleToTakeDamage) return;
            if (Durability == 0) return;

            Durability -= damage;
            if (Durability <= 0)
            {
                Durability = 0;
                Delete();

            }
        }

        public virtual void Delete()
        {
            try
            {
                Application.Current.Update -= Update;
                Myastro.Current.Scene.RemoveChild(Node);
                Deleted = true;
                _entityWasDeleted?.Invoke(this);
            }
            catch (NullReferenceException) { }
            catch (InvalidOperationException) { }
        }

    }
}





