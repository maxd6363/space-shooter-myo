﻿using Urho;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Ship
{
    public abstract class Flame : Entity
    {
        protected Entity AttachedTo { get; }

        public Flame(Entity attachedTo) : base(false, false)
        {
            AttachedTo = attachedTo;
            AttachedTo.Node.CreateChild("Flame").AddComponent(this);
            Sprite = Node.CreateComponent<StaticSprite2D>();
            Node.Position = new Vector3(10, 10, 0);
        }

        /// <summary>
        /// Power animation
        /// </summary>
        /// <param name="power">between 0.0 and 1.0 </param>
        public abstract void SetPower(float power);

    }
}
