﻿using System;
using System.Diagnostics;
using Urho;
using Urho.Actions;
using Urho.Urho2D;
using UrhoApplication.Entity.Projectil.Laser;
using UrhoApplication.Utils;



namespace UrhoApplication.Entity.Ship
{
    public class CommonShip : Ship
    {

        private float counterFire = 0;
        private readonly SimpleFlame Flame1;
        private readonly SimpleFlame Flame2;



        public CommonShip(uint playerCount) : base()
        {
            Shape = Node.CreateComponent<CollisionCircle2D>();
            (Shape as CollisionCircle2D).Radius = AppConfig.Current.ShipShapeRadius;
            (Shape as CollisionCircle2D).Density = AppConfig.Current.ShipShapDensity;
            (Shape as CollisionCircle2D).Center = new Vector2(0, AppConfig.Current.ShipShapeMainMarginY);

            Shape1 = Node.CreateComponent<CollisionCircle2D>();
            (Shape1 as CollisionCircle2D).Radius = AppConfig.Current.ShipShapeRadius / 2f;
            (Shape1 as CollisionCircle2D).Density = AppConfig.Current.ShipShapDensity;
            (Shape1 as CollisionCircle2D).Center = new Vector2(AppConfig.Current.ShipShapeAuxMarginX, AppConfig.Current.ShipShapeAuxMarginY);

            Shape2 = Node.CreateComponent<CollisionCircle2D>();
            (Shape2 as CollisionCircle2D).Radius = AppConfig.Current.ShipShapeRadius / 2f;
            (Shape2 as CollisionCircle2D).Density = AppConfig.Current.ShipShapDensity;
            (Shape2 as CollisionCircle2D).Center = new Vector2(-AppConfig.Current.ShipShapeAuxMarginX, AppConfig.Current.ShipShapeAuxMarginY);

            



            Node.Scale = 0.4f * Vector3.One;


            Flame1 = new SimpleFlame(this, new Vector2(2, -2.6f));
            Flame2 = new SimpleFlame(this, new Vector2(-2, -2.6f));


            var _attributs = typeof(Assets.Sprites.Ships).GetFields();
            var _spriteToLoad = (string)_attributs[playerCount % _attributs.Length].GetValue(null);
            Sprite.Sprite = Application.ResourceCache.GetSprite2D(_spriteToLoad);

            RandomRotation();
            RandomPosition();
            PushImpulse();
            DoSpawnAnimation();
        }



        public override void ThrusterForward(float power)
        {
            if (Dead) return;
            Body.ApplyForceToCenter(ToolBox.DegreeToVectorNomalized(Node.Rotation2D + 90) * power * SpeedRatio, true);

            Flame1.SetPower(MathHelper.Clamp(power / AppConfig.Current.ShipThrusterBaseSpeed,0,1));
            Flame2.SetPower(MathHelper.Clamp(power / AppConfig.Current.ShipThrusterBaseSpeed,0,1));

        }

        public override void ShipRotation(float degree)
        {
            if (Dead) return;
            Body.ApplyAngularImpulse(degree, true);
        }

        public override void FireLaser()
        {
            if (Dead) return;
            if (counterFire <= 0)
            {
                Sound.Play(Application.Current.ResourceCache.GetSound(ToolBox.Random.Next(0, 2) == 0 ? Assets.Sounds.Laser1 : Assets.Sounds.Laser2));
                new SimpleLaser(this);
                counterFire += (AppConfig.Current.ShipDefaultFiringThrottling / Myastro.Current.Time.TimeStep);
            }



        }


        public override void Update(UpdateEventArgs obj)
        {
            base.Update(obj);
            if (counterFire > 0)
                counterFire--;
            LimitRotation();
            LimitVelocity();
        }


        private void LimitRotation()
        {

            try
            {
                if (Body.AngularVelocity > AppConfig.Current.ShipAngularVelocityMargin)
                    Body.AngularVelocity -= AppConfig.Current.ShipAngularVelocityReset;
                if (Body.AngularVelocity < AppConfig.Current.ShipAngularVelocityMargin)
                    Body.AngularVelocity += AppConfig.Current.ShipAngularVelocityReset;
            }
            catch (InvalidOperationException)
            {

            }
        }

        private void LimitVelocity()
        {

            try
            {
                if (Body.LinearVelocity.Length != 0)
                    Body.ApplyLinearImpulseToCenter(new Vector2(-Body.LinearVelocity.X, -Body.LinearVelocity.Y) / AppConfig.Current.ShipLinearVelocityRatio, true);
            }
            catch (InvalidOperationException)
            {

            }
        }


        protected override async void DoSpawnAnimation()
        {
            var _nodeScale = Node.Scale;
            Node.Scale = Vector3.Zero;
            await Node.RunActionsAsync(new EaseInOut(new ScaleTo(AppConfig.Current.ShipSpawnAnimationTiming, _nodeScale.X, _nodeScale.Y, _nodeScale.Y), 1));
        }


        public override void TakeDamage(float damage)
        {
            if (!AbleToTakeDamage) return;
            if (Dead) return;
            Durability -= damage;
            if (Durability <= 0)
            {
                Durability = 0;
                Die();
            }
            Sound.Play(Application.Current.ResourceCache.GetSound(Assets.Sounds.ShieldDown));
            Myastro.Current.Camera.Rumble();
        }

        public override void Die()
        {
            base.Die();
            Node.RemoveComponent(Flame1);
            Node.RemoveComponent(Flame2);

            Dead = true;
            Sprite.Alpha = AppConfig.Current.ShipDeadOpacity;
            Sound.Play(Application.Current.ResourceCache.GetSound(Assets.Sounds.Lose));
        }
    }
}
