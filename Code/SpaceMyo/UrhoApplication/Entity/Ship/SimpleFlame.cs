﻿using System;
using System.Diagnostics;
using Urho;
using Urho.Actions;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Ship
{
    public class SimpleFlame : Flame
    {
        private Vector2 _initalPosition;
        private Vector3 _initalScale;
        private ParticleEmitter2D ParticleEmitter;

        public SimpleFlame(Entity attachedTo, Vector2 position) : base(attachedTo)
        {
            Sprite.Sprite = Application.ResourceCache.GetSprite2D(Assets.Sprites.Flame.BigBlue);
            Node.Position = new Vector3(position);

            _initalPosition = position;
            _initalScale = Vector3.One * 1.3f;
            Node.Scale = _initalScale;
            var particle = Application.ResourceCache.GetParticleEffect2D(Assets.Particles.Sun);
            ParticleEmitter = Node.CreateComponent<ParticleEmitter2D>();
            ParticleEmitter.Effect = particle;
            ParticleEmitter.Emitting = true;
            ParticleEmitter.MaxParticles = 20000;
            

        }


        public override void SetPower(float power)
        {
            power = MathHelper.Clamp(power, 0.1f, 1);
            SetAlpha(power);
            Rumble(power);
            Expend(power);
        }

        private void Expend(float power)
        {
            Node.Scale = new Vector3(_initalScale.X, _initalScale.Y * (power + 1), _initalScale.Z);
        }

        private void SetAlpha(float power)
        {
            Sprite.Alpha = power;
        }

        private void Rumble(float power)
        {
            var _x = ToolBox.RandomFloat(_initalPosition.X - (AppConfig.Current.FlameRumbleRatio * power), _initalPosition.X + (AppConfig.Current.FlameRumbleRatio * power));
            var _y = ToolBox.RandomFloat(_initalPosition.Y - (AppConfig.Current.FlameRumbleRatio * power), _initalPosition.Y + (AppConfig.Current.FlameRumbleRatio * power));
            Node.Position = new Vector3(_x, _y, 0);
        }

    }
}
