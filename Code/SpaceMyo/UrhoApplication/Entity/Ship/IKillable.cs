﻿namespace UrhoApplication.Entity.Ship
{
    public interface IKillable
    {
        void Die();
    }
}
