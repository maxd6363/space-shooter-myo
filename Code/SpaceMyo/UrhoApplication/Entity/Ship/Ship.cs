﻿using System;
using Urho;
using Urho.Actions;
using Urho.Urho2D;
using UrhoApplication.EntityGraphic;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Ship
{
    public abstract class Ship : Entity, IKillable
    {
        public bool Dead { get; protected set; }
        public float SpeedRatio { get; set; }

        protected SoundCustom Sound { get; }
        protected CollisionShape2D Shape1 { get; set; }
        protected CollisionShape2D Shape2 { get; set; }
        

        public event EventHandler ShipDied;

        
        public Ship() : base()
        {
     
            Sound = Node.CreateComponent<SoundCustom>();
            Durability = AppConfig.Current.ShipDurability;
            Dead = false;
            SpeedRatio = 1f;


        }
        public override void Update(UpdateEventArgs obj)
        {
            base.Update(obj);
        }

    


        public abstract void ThrusterForward(float power);
        public abstract void ShipRotation(float degree);
        public abstract void FireLaser();
        public virtual async void Die()
        {
            ShipDied?.Invoke(this, null);
            Body.Enabled = false;
            await Node.RunActionsAsync(new EaseIn(new ScaleTo(AppConfig.Current.ShipDeadDurationAnimation, 0), 1f));
        }




    }
}
