﻿using System;
using System.Collections.Generic;
using System.Text;
using UrhoApplication.Entity.Bonus.Effect;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Bonus
{
    class BonusHeal : Bonus
    {
        public BonusHeal(BonusForceEnum bonusForce) : base(bonusForce)
        {
            string _spriteToLoad;
            switch (Force)
            {
                case BonusForceEnum.BRONZE:
                    _spriteToLoad = Assets.Sprites.Bonus.Pill.Blue;
                    break;
                case BonusForceEnum.SILVER:
                    _spriteToLoad = Assets.Sprites.Bonus.Pill.Green;
                    break;
                default:
                    _spriteToLoad = Assets.Sprites.Bonus.Pill.Red;
                    break;
            }

            Sprite.Sprite = Application.ResourceCache.GetSprite2D(_spriteToLoad);



        }

        public override void AddEffect(Entity entity)
        {

            Effect = new EffectHeal(entity, Force);
            base.AddEffect(entity);
        }
    }
}
