﻿using UrhoApplication.Entity.Bonus.Effect;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Bonus
{
    public class BonusShield : Bonus
    {
        public BonusShield(BonusForceEnum bonusForce) : base(bonusForce)
        {
            string _spriteToLoad;
            switch (Force)
            {
                case BonusForceEnum.BRONZE:
                    _spriteToLoad = Assets.Sprites.Bonus.Shield.Bronze;
                    break;
                case BonusForceEnum.SILVER:
                    _spriteToLoad = Assets.Sprites.Bonus.Shield.Silver;
                    break;
                default:
                    _spriteToLoad = Assets.Sprites.Bonus.Shield.Gold;
                    break;
            }

            Sprite.Sprite = Application.ResourceCache.GetSprite2D(_spriteToLoad);


        }

        public override void AddEffect(Entity entity)
        {
            Effect = new EffectShield(entity, Force);
            base.AddEffect(entity);
        }
    }
}
