﻿using System;
using System.Diagnostics;
using Urho;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Bonus
{
    public abstract class Bonus : Entity
    {

        public float AvalaibleTicks { get; protected set; }
        public BonusForceEnum Force { get; }

        protected Effect.Effect Effect { get; set; }


        public Bonus(BonusForceEnum bonusForce) : base()
        {
            Force = bonusForce;
            Shape = Node.CreateComponent<CollisionBox2D>();
            (Shape as CollisionBox2D).Size = Vector2.One * 2;
            Durability = AppConfig.Current.BonusIconDurability;

            RandomPosition();
            DoSpawnAnimation();

        }


        public override void Update(UpdateEventArgs obj)
        {
            if (Deleted) return;

            Durability -= obj.TimeStep;
            if (Durability <= 0)
                Delete();
        }


        public override void TakeDamage(float damage)
        {
        }


        public override void RandomPosition()
        {
            var _player = Myastro.Current.ManagerSingleplayer.Player;
            Vector3 _center;
            try
            {
                _center = _player == null ? Vector3.Zero : _player.ControlledShip.Node.Position;
            }
            catch (InvalidOperationException)
            {
                _center = Vector3.Zero;
            }
            Node.Position = new Vector3(ToolBox.RandomFloat(_center.X + AppConfig.Current.EndOfMap_MinusX, _center.X + AppConfig.Current.EndOfMap_PlusX), ToolBox.RandomFloat(_center.Y + AppConfig.Current.EndOfMap_MinusY, _center.Y + AppConfig.Current.EndOfMap_PlusY), 0);
        }




        public virtual void AddEffect(Entity entity)
        {
            if (Effect != null)
                Effect.EntityWasDeleted += EntityDeleted;
        }


        private void EntityDeleted(Entity obj)
        {
            Myastro.Current.ManagerBonus.Bonus.Remove(this);
        }


        public override void Delete()
        {
            base.Delete();
            Myastro.Current.ManagerBonus.Bonus.Remove(this);
        }

    }
}
