﻿using System;
using System.Diagnostics;
using Urho;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Bonus.Effect
{
    class EffectSpeed : Effect
    {



        public EffectSpeed(Entity attachedTo, BonusForceEnum force) : base(attachedTo, force)
        {

            (AttachedTo as Ship.Ship).SpeedRatio = 1.5f;

        }

    
        public override void Delete()
        {
            base.Delete();
            (AttachedTo as Ship.Ship).SpeedRatio = 1f;
        }
    }
}
