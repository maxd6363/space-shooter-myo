﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Urho;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Bonus.Effect
{
    public abstract class Effect : Entity
    {

        public BonusForceEnum Force { get; }
        public Entity AttachedTo { get; }



        public Effect(Entity attachedTo, BonusForceEnum force) : base(false, false)
        {
            AttachedTo = attachedTo;
            Force = force;

            Application.Current.Update += Update;

            switch (Force)
            {
                case BonusForceEnum.BRONZE:
                    Durability = AppConfig.Current.BonusDurabilityBronze;
                    break;
                case BonusForceEnum.SILVER:
                    Durability = AppConfig.Current.BonusDurabilitySilver;
                    break;
                case BonusForceEnum.GOLD:
                    Durability = AppConfig.Current.BonusDurabilityGold;
                    break;
            }
        }

        public override void Update(UpdateEventArgs obj)
        {
            if (Deleted) return;

            Durability -= obj.TimeStep;
            if (Durability <= 0)
                Delete();

        }


        public override void Delete()
        {
            base.Delete();
        }

    }
}

