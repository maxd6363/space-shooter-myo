﻿using System;
using System.Diagnostics;
using Urho;
using Urho.Urho2D;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Bonus.Effect
{
    class EffectShield : Effect
    {
        
        public EffectShield(Entity attachedTo, BonusForceEnum force) : base(attachedTo, force)
        {

            AttachedTo.Node.CreateChild("ShieldEffect").AddComponent(this);

            Sprite = Node.CreateComponent<StaticSprite2D>();
            Sprite.Sprite = Application.ResourceCache.GetSprite2D(Assets.Sprites.Bonus.Shield.Effect1);

            (AttachedTo.Shape as CollisionCircle2D).Radius = AppConfig.Current.ShieldShapeSize;
            (AttachedTo.Shape as CollisionCircle2D).Density = AppConfig.Current.ShieldShapDensity;
            AttachedTo.AbleToTakeDamage = false;

         

        }

    

        public override void Delete()
        {
            base.Delete();
            try
            {
                (AttachedTo.Shape as CollisionCircle2D).Radius = AppConfig.Current.ShipShapeRadius;
                (AttachedTo.Shape as CollisionCircle2D).Density = AppConfig.Current.ShipShapDensity;
                AttachedTo.AbleToTakeDamage = true;
                AttachedTo.Node.RemoveChild(Node);

            }
            catch (NullReferenceException){ }
            catch (InvalidOperationException){ }
        }
    }
}
