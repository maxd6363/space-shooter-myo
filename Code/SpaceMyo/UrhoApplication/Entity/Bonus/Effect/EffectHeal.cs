﻿using System;
using System.Collections.Generic;
using System.Text;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Bonus.Effect
{
    public class EffectHeal : Effect
    {
        public EffectHeal(Entity attachedTo, BonusForceEnum force) : base(attachedTo, force)
        {
            AttachedTo.Durability += AppConfig.Current.HealEffectDurabilityAdded * (int)(force + 1);
        }






    }
}
