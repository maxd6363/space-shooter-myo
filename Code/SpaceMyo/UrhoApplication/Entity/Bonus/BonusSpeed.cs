﻿using UrhoApplication.Entity.Bonus.Effect;
using UrhoApplication.Utils;

namespace UrhoApplication.Entity.Bonus
{
    public class BonusSpeed : Bonus
    {
        public BonusSpeed(BonusForceEnum bonusForce) : base(bonusForce)
        {
            string _spriteToLoad;
            switch (Force)
            {
                case BonusForceEnum.BRONZE:
                    _spriteToLoad = Assets.Sprites.Bonus.Power.Bronze;
                    break;
                case BonusForceEnum.SILVER:
                    _spriteToLoad = Assets.Sprites.Bonus.Power.Silver;
                    break;
                default:
                    _spriteToLoad = Assets.Sprites.Bonus.Power.Gold;
                    break;
            }

            Sprite.Sprite = Application.ResourceCache.GetSprite2D(_spriteToLoad);



        }

        public override void AddEffect(Entity entity)
        {
            Effect = new EffectSpeed(entity, Force);
        }
    }
}
