﻿using Urho;
using Urho.Urho2D;

namespace UrhoApplication.EntityGraphic
{
    public class Scene2DPhysics : Scene
    {
        public Scene2DPhysics() : base()
        {
            CreateComponent<Octree>();
            var _physics = CreateComponent<PhysicsWorld2D>();
            _physics.Gravity = Vector2.Zero;
            CreateComponent<Physics2DCollisionHandler>().PhysicsWorld2D = _physics;
            CreateComponent<DebugRenderer>();
            UpdateEnabled = true;            
        }
    }
}
