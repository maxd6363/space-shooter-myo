﻿using Urho.Audio;
using Urho;
using UrhoApplication.Utils;
using System.Reflection;
using System.Collections.Generic;
using System.Diagnostics;

namespace UrhoApplication.EntityGraphic
{
    public class MusicHandler : SoundSource
    {
        private IList<string> Playlist { get; }

        public MusicHandler()
        {
            Myastro.Current.Scene.CreateChild("MusicHandler").AddComponent(this);

            Playlist = new List<string>();

            FieldInfo[] _attributs = typeof(Assets.Sounds.Musics).GetFields();
            foreach (FieldInfo field in _attributs)
            {
                Playlist.Add(field.GetValue(null) as string);
            }
            Gain = Myastro.Current.GameSettings.MusicGain;
            Myastro.Current.GameSettings.MusicGainChanged += MusicGainChanged;

            RandomPlay();


        }

        private void MusicGainChanged(float obj)
        {
            Gain = obj;
        }

        private void RandomPlay()
        {
            Play(Application.Current.ResourceCache.GetSound(Playlist.GetRandomElement()));
        }

    }
}
