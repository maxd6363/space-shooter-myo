﻿using Urho;
using Urho.Urho2D;
using UrhoApplication.Entity.Bonus;
using UrhoApplication.Entity.Projectil.Laser;
using UrhoApplication.Entity.Ship;
using UrhoApplication.Utils;

namespace UrhoApplication.EntityGraphic
{
    class Physics2DCollisionHandler : Component
    {

        public PhysicsWorld2D PhysicsWorld2D
        {
            get => _physicsWorld2D;
            set
            {
                if (_physicsWorld2D != value)
                {
                    _physicsWorld2D = value;
                    _physicsWorld2D.PhysicsBeginContact2D += PhysicsWorld2D_PhysicsBeginContact2D;
                }
            }
        }

        private PhysicsWorld2D _physicsWorld2D;

        public Physics2DCollisionHandler()
        {

        }


        private void PhysicsWorld2D_PhysicsBeginContact2D(PhysicsBeginContact2DEventArgs obj)
        {

            bool _addScore = false;
            Bonus bonusCollided;




            Laser laserCollided = GetLaserFromCollision(obj);
          
             

            Entity.Entity otherEntity = GetEntityFromCollision(obj);
            if (otherEntity != null)
            {
                float _damageInflicted = 0;
                if (laserCollided != null && otherEntity != laserCollided.AttachedTo) { 
                    laserCollided.Delete();
                    _damageInflicted = laserCollided.DamageInflicted;
                    _addScore = true;
                    otherEntity.TakeDamage(_damageInflicted);
                }


                bonusCollided = GetBonusFromCollision(obj);
                if (bonusCollided != null && laserCollided == null && otherEntity.GetType().IsSubclassOf(typeof(Ship)))
                {
                    bonusCollided.Delete();
                    bonusCollided.AddEffect(otherEntity);
                }

            }

            
            
            if (_addScore && Myastro.Current.ManagerSingleplayer.Player != null)
                Myastro.Current.ManagerSingleplayer.Player.Score += AppConfig.Current.PlayerAddScore;



        }

        private Entity.Entity GetEntityFromCollision(PhysicsBeginContact2DEventArgs obj)
        {
            Entity.Entity otherEntity = null;
            if (obj.BodyA.GetComponent<Entity.Entity>().GetType().IsSubclassOf(typeof(Entity.Entity)))
            {
                otherEntity = obj.BodyA.GetComponent<Entity.Entity>();
            }

            if (obj.BodyB.GetComponent<Entity.Entity>().GetType().IsSubclassOf(typeof(Entity.Entity)))
            {
                otherEntity = obj.BodyB.GetComponent<Entity.Entity>();
            }
            if (obj.NodeA.GetComponent<Entity.Entity>().GetType().IsSubclassOf(typeof(Entity.Entity)))
            {
                otherEntity = obj.NodeA.GetComponent<Entity.Entity>();
            }

            if (obj.NodeB.GetComponent<Entity.Entity>().GetType().IsSubclassOf(typeof(Entity.Entity)))
            {
                otherEntity = obj.NodeB.GetComponent<Entity.Entity>();
            }
            return otherEntity;
        }

        private Laser GetLaserFromCollision(PhysicsBeginContact2DEventArgs obj)
        {
            Laser laserCollided = null;

            if (obj.NodeA.GetComponent<Entity.Entity>().GetType() == typeof(SimpleLaser))
            {
                laserCollided = obj.NodeA.GetComponent<Entity.Entity>() as Laser;
            }
            if (obj.NodeB.GetComponent<Entity.Entity>().GetType() == typeof(SimpleLaser))
            {
                laserCollided = obj.NodeB.GetComponent<Entity.Entity>() as Laser;
            }
            return laserCollided;
        }

        private Bonus GetBonusFromCollision(PhysicsBeginContact2DEventArgs obj)
        {
            Bonus bonusCollided = null;

            if (obj.NodeA.GetComponent<Entity.Entity>().GetType().IsSubclassOf(typeof(Bonus)))
            {
                bonusCollided = obj.NodeA.GetComponent<Entity.Entity>() as Bonus;
            }
            if (obj.NodeB.GetComponent<Entity.Entity>().GetType().IsSubclassOf(typeof(Bonus)))
            {
                bonusCollided = obj.NodeB.GetComponent<Entity.Entity>() as Bonus;
            }
            return bonusCollided;
        }




    }
}
