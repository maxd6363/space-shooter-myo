﻿using Urho;
using UrhoApplication.Utils;

namespace UrhoApplication.EntityGraphic
{
    public class SpaceBackground : Component
    {
        protected StaticModel Model;

        public SpaceBackground()
        {
            Myastro.Current.Scene.CreateChild().AddComponent(this);


            Node.Position = new Vector3(0, 0, AppConfig.Current.BackgroundZ);
            Node.Scale = new Vector3(AppConfig.Current.BackgroundScale, AppConfig.Current.BackgroundScale, 1);

            Model = Node.CreateComponent<StaticModel>();
            Model.Model = Application.Current.ResourceCache.GetModel(Assets.Models.Box);
            Model.Material = Application.Current.ResourceCache.GetMaterial(AppConfig.Current.DefaultBackground);
            Model.Material.SetUVTransform(Vector2.Zero, 0, AppConfig.Current.DefaultBackgroundUVRepeat);
        }
    



    }
}
