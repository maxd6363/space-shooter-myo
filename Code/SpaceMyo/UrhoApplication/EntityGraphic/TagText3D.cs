﻿using Urho;
using Urho.Gui;
using UrhoApplication.Entity;
using UrhoApplication.Utils;

namespace UrhoApplication.EntityGraphic
{
    public class TagText3D : Text3D, IDeletable
    {
        public bool ThwartDirection { get; private set; }
        private Entity.Entity AttachedTo { get; }
        private string Tag { get; }


        public TagText3D(Entity.Entity attachTo, string tag, int numPlayer, bool thwartDirection = true)
        {
            ThwartDirection = thwartDirection;
            AttachedTo = attachTo;
            Tag = tag;
            AttachedTo.Node.CreateChild().AddComponent(this);

            SetFont(Assets.Fonts.AnonymousPro, AppConfig.Current.DefaultTagFontSize);
            SetColor(AppConfig.Current.DefaultPlayerColor[numPlayer % AppConfig.Current.DefaultPlayerColor.Count]);
            Node.SetPosition2D(AppConfig.Current.DefaultTagMargin);
            HorizontalAlignment = HorizontalAlignment.Center;
            VerticalAlignment = VerticalAlignment.Center;
            TextAlignment = HorizontalAlignment.Center;
            Text = $"{Tag}\n{(100 * AttachedTo.Durability / AppConfig.Current.ShipDurability):F0}";

            Node.Scale *= AppConfig.Current.DefaultTagScaleRatio;
            Application.Current.Update += Update;
        }

        private void Update(UpdateEventArgs obj)
        {
            Text = $"{Tag}\n{(100 * AttachedTo.Durability / AppConfig.Current.ShipDurability):F0}";
            if (!ThwartDirection) return;
            Node.Rotation2D = -AttachedTo.Node.Rotation2D;
        }

        public void Delete()
        {
            Application.Current.Update -= Update;
            AttachedTo.Node.RemoveComponent(this);
        }
    }
}
