﻿using Urho;
using Urho.Gui;
using UrhoApplication.Entity;
using UrhoApplication.Utils;

namespace UrhoApplication.EntityGraphic
{
    public class Cursor : BorderImage, IDeletable
    {
        public Cursor() : base()
        {
            Application.Current.UI.Root.AddChild(this);
            Application.Current.Renderer.BeginViewRender += BeginViewRender;
            Application.Current.Renderer.RenderSurfaceUpdate += RenderSurfaceUpdate;
            Application.Current.Input.SetMouseVisible(true);

            Texture = Application.Current.ResourceCache.GetTexture2D(Assets.Textures.Cursor);
            Size = new IntVector2(50, 50);
            Opacity = 3f;
        }

        private void RenderSurfaceUpdate(RenderSurfaceUpdateEventArgs obj)
        {
            Position = Application.Current.Input.MousePosition;
        }

        private void BeginViewRender(BeginViewRenderEventArgs obj)
        {
            Position = Application.Current.Input.MousePosition;
        }

        public void Delete()
        {
            Application.Current.Renderer.BeginViewRender -= BeginViewRender;
            Application.Current.Renderer.RenderSurfaceUpdate -= RenderSurfaceUpdate;
            Application.Current.UI.Root.RemoveChild(this);
        }
    }
}
