﻿using Urho;
using UrhoApplication.Utils;

namespace UrhoApplication.EntityGraphic
{
    public class ViewportCustom : Viewport
    {
        public ViewportCustom() : base(Application.Current.Context, Myastro.Current.Scene, Myastro.Current.Camera)
        {

            Application.Current.Renderer.SetViewport(0, this);
            Application.Current.Renderer.HDRRendering = true;
            var rp = RenderPath.Clone();

            foreach(string postProcess in AppConfig.Current.PostProcessing)
            {
                rp.Append(Application.Current.ResourceCache.GetXmlFile(postProcess));
            }

            RenderPath = rp;


        }
    }
}
