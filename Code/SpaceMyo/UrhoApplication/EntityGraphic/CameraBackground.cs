﻿using System.Diagnostics;
using Urho;
using Urho.Actions;
using UrhoApplication.Utils;

namespace UrhoApplication.EntityGraphic
{
    public class CameraBackground : Camera
    {
        public Entity.Entity Following { get; set; }
        protected SpaceBackground Background;

        private float Counter;
        private bool IsRumbling;

        public CameraBackground(Entity.Entity following = null) : base()
        {
            Following = following;
            Myastro.Current.Scene.CreateChild().AddComponent(this);
            Zoom /= 2;
            Node.Position = new Vector3(0, 0, AppConfig.Current.CameraZ);
            Orthographic = true;
            Background = new SpaceBackground();
            Application.Current.Update += Update;
            Counter = AppConfig.Current.CameraRumbleCounter;
            IsRumbling = false;
        }

        private void Update(UpdateEventArgs obj)
        {
            
            if (Counter <= 0 && IsRumbling == true) IsRumbling = false;


            if (IsRumbling)
            {
                var _pos = Node.Position;
                var _delta = AppConfig.Current.CameraRumbleScale;
                Node.Position = new Vector3(ToolBox.RandomFloat(_pos.X - _delta, _pos.Y + _delta), ToolBox.RandomFloat(_pos.Y - _delta, _pos.Y + _delta), _pos.Z);
                Counter -= obj.TimeStep;
                return;
            }

            if (Following != null)
            {
                Node.Position =  new Vector3(Following.Node.Position.X, Following.Node.Position.Y, AppConfig.Current.CameraZ);
                //Node.LookAt(Following.Node.Position, Vector3.Up);
            }

        }

        public async void Reset()
        {
            await Node.RunActionsAsync(new EaseBackOut(new MoveTo(1.5f, new Vector3(0, 0, AppConfig.Current.CameraZ)))); 
        }

        public void Rumble()
        {
            IsRumbling = true;
            Counter = AppConfig.Current.CameraRumbleCounter;

        }
    }
}
