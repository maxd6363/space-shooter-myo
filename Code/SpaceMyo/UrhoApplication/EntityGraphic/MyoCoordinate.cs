﻿using MyoLib;
using MyoSharp.Device;
using System;
using System.Collections.Generic;
using System.Text;
using Urho;
using Urho.Gui;
using UrhoApplication.Utils;

namespace UrhoApplication.EntityGraphic
{
    class MyoCoordinate : Text
    {
        private float x, y = 0;
        private MyoManager man;
        public MyoCoordinate(MyoManager manager) : this(UIPosition.TOP_RIGHT, manager)
        {
        }

        public MyoCoordinate(UIPosition pos, MyoManager manager)
        {
            man = manager;
            Application.Current.Update += Update;
            SetColor(AppConfig.Current.DefaultFPSColor);
            SetFont(Application.Current.ResourceCache.GetFont(Assets.Fonts.AnonymousPro), AppConfig.Current.DefaultFPSFontSize);
            Application.Current.UI.Root.AddChild(this);

        }

        private void Coordinate(OrientationDataEventArgs e)
        {
            x = (float)e.Pitch;
            y = (float)e.Yaw;
        }

        private void Update(UpdateEventArgs obj)
        {
            man.SubscribeToOrientationData(0, (source, args) => Coordinate(args));
            Value = $"X : {x} ; Y: {y}";
        }
    }
}
