﻿using Urho.Audio;
using Urho;

namespace UrhoApplication.EntityGraphic
{
    public class SoundCustom : SoundSource
    {
        public SoundCustom()
        {
            Gain = Myastro.Current.GameSettings.SoundGain;
            Myastro.Current.GameSettings.SoundGainChanged += SoundGainChanged;
        }

        private void SoundGainChanged(float obj)
        {
            Gain = obj;
        }
    }
}
