﻿using System;
using System.Diagnostics;
using Urho;
using UrhoApplication.Player;
using UrhoApplication.Utils;

namespace UrhoApplication.Manager
{
    public class ManagerSingleplayer
    {
        public IPlayer Player { get; private set; }

        private static bool SingleplayerRunning = false;
        private readonly GameSettings Settings;
        private readonly Myastro App;

        public ManagerSingleplayer()
        {
            App = Application.Current as Myastro;
            Settings = App.GameSettings;
        }


        public void StartSingleplayer()
        {
            if (!SingleplayerRunning)
            {
                Player = new SimplePlayer(Settings.SingleplayerInput);
                Player.ControlledShip.Wrapping = false;
                App.Camera.Following = Player.ControlledShip;
                App.ManagerBonus.Start();
                App.ManagerBonus.MultiplayerMode = false;
                App.ManagerAsteroids.SetModeSingleplayer();
                SingleplayerRunning = true;
            }

        }


        public void StopSingleplayer()
        {
            if (SingleplayerRunning)
            {
                try
                {
                    Player.Delete();
                    SimplePlayer.ResetPlayerCount();
                    App.Camera.Following = null;
                    App.Camera.Reset();
                    App.ManagerBonus.Stop();
                    App.ManagerAsteroids.SetModeMultiplayer();
                    SingleplayerRunning = false;
                }
                catch (Exception)
                {
                    SingleplayerRunning = false;
                }
            }

        }

    }
}

