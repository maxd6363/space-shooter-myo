﻿using System.Collections.Generic;
using System.Diagnostics;
using UrhoApplication.Input.UWP.Myo;

namespace UrhoApplication.Manager
{
    public class ManagerMyos
    {
        /// <summary>
        /// list of myos in the game
        /// </summary>
        private IList<MyoInput> _listMyos;

        /// <summary>
        /// Constructor
        /// </summary>
        public ManagerMyos()
        {
            _listMyos = new List<MyoInput>();
        }

        /// <summary>
        /// Adding of one myo
        /// </summary>
        /// <param name="myoId">myo's id</param>
        /// <returns>myo ajouté</returns>
        public MyoInput AddMyo(int myoId)
        {
            var _myo = new MyoInput(myoId);
            _listMyos.Add(_myo);

            Debug.WriteLine($"{_listMyos.Count}");
            return _myo;
        }

        /// <summary>
        /// Flush the list of myo
        /// </summary>
        public void Flush()
        {
            _listMyos.Clear();
        }

        /// <summary>
        /// Myos's recalibration during the game
        /// </summary>
        public void SynchronizationMyos()
        {
            Debug.WriteLine($"{_listMyos.Count}");
            foreach(MyoInput myo in _listMyos)
            {
                myo.SynchronizeMyMyo();
            }
        }
    }
}
