﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Urho;
using UrhoApplication.Entity.Bonus;
using UrhoApplication.Factory;
using UrhoApplication.Utils;

namespace UrhoApplication.Manager
{
    public class ManagerBonus
    {
        public IList<Bonus> Bonus { get; }
        public AbstractFactory Factory { get; }
        public bool MultiplayerMode { get; set; }

        protected float Tick;
        protected int MaxBonus;


        public ManagerBonus()
        {
            Bonus = new List<Bonus>();
            Factory = new SimpleFactory();
            MultiplayerMode = false;
            Tick = ToolBox.RandomFloat(0.1f, AppConfig.Current.BonusTickRate);
            MaxBonus = AppConfig.Current.BonusMaxCount;


        }

        public void Start()
        {
            Application.Current.Update += Update;
        }


        public void Stop()
        {
            Application.Current.Update -= Update;
            foreach (Bonus bonus in Bonus)
            {
                bonus.Delete();
            }
        }




        private void Update(UpdateEventArgs obj)
        {
            Tick -= obj.TimeStep;
            if (Tick <= 0)
            {
                Tick = ToolBox.RandomFloat(0.1f, AppConfig.Current.BonusTickRate);
                if (Bonus.Count < MaxBonus)
                    AddBonus();
            }



        }

        private void AddBonus()
        {
            var _bonus = Factory.CreateBonus();
            _bonus.Wrapping = MultiplayerMode;
            Bonus.Add(_bonus);

        }

    }
}
