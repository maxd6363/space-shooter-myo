﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Urho;
using UrhoApplication.Entity;
using UrhoApplication.Input;
using UrhoApplication.Player;
using UrhoApplication.Utils;

namespace UrhoApplication.Manager
{
    public class ManagerMultiplayer
    {
        public IList<IPlayer> Players { get; }


        private static bool MultiplayerRunning = false;
        private readonly GameSettings Settings;
        private readonly Myastro App;

        public ManagerMultiplayer()
        {
            App = Application.Current as Myastro;
            Settings = App.GameSettings;
            Players = new List<IPlayer>();

        }


        public void StartMultiplayer()
        {
            if (!MultiplayerRunning)
            {
                foreach (ImplementedInputs input in Settings.MultiplayerInputs)
                {
                    Players.Add(new SimplePlayer(input));
                }
                App.ManagerBonus.Start();
                App.ManagerBonus.MultiplayerMode = true;
                App.ManagerAsteroids.SetModeMultiplayer();
                MultiplayerRunning = true;
            }

        }


        public void StopMultiplayer()
        {
            if (MultiplayerRunning)
            {
                try
                {
                    foreach (IDeletable player in Players)
                    {
                        player.Delete();
                    }
                    Players.Clear();
                    SimplePlayer.ResetPlayerCount();
                    App.ManagerBonus.Stop();
                    App.GameSettings.ResetMultiplayerInput();
                    MultiplayerRunning = false;

                }
                catch (Exception)
                {
                    MultiplayerRunning = false;
                }
            }
        }



    }
}
