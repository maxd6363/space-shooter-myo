﻿using System.Collections.Generic;
using UrhoApplication.Entity.Asteroid;
using UrhoApplication.Factory;
using UrhoApplication.Utils;

namespace UrhoApplication.Manager
{
    public class ManagerAsteroids
    {
        public IList<Asteroid> Asteroids { get; }
        public AbstractFactory Factory { get; }


        /// <summary>
        /// ctor for managerAstroids
        /// </summary>
        public ManagerAsteroids()
        {

            Asteroids = new List<Asteroid>();
            Factory = new SimpleFactory();


            for (int i = 0; i < AppConfig.Current.NumberOfAsteroidsAverage; ++i)
            {
                var astroid = Factory.CreateAsteroid();
                astroid.EntityWasDeleted += Astroid_EntityWasDeleted;
                Asteroids.Add(astroid);
            }

        }

        private void Astroid_EntityWasDeleted(Entity.Entity obj)
        {
            var asteroidDeleted = obj as Asteroid;
            if (asteroidDeleted != null)
            {
                asteroidDeleted.EntityWasDeleted -= Astroid_EntityWasDeleted;
                Asteroids.Remove(asteroidDeleted);
                var astroidAdded = Factory.CreateAsteroid();
                astroidAdded.EntityWasDeleted += Astroid_EntityWasDeleted;
                Asteroids.Add(astroidAdded);
            }

        }


        public void SetModeSingleplayer()
        {
            foreach (Entity.Entity entity in Asteroids)
            {
                entity.Wrapping = false;
            }
        }

        public void SetModeMultiplayer()
        {
            foreach (Entity.Entity entity in Asteroids)
            {
                entity.Wrapping = true;
            }
        }

    }
}
