﻿using System;
using UrhoApplication.Entity.Asteroid;
using UrhoApplication.Entity.Bonus;
using UrhoApplication.Utils;

namespace UrhoApplication.Factory
{
    public class SimpleFactory : AbstractFactory
    {

        public override Asteroid CreateAsteroid(AsteroidSize size)
        {
            switch (size)
            {
                #pragma warning disable CS0618 // Type or member is obsolete
                case AsteroidSize.TINY:
                    return new SimpleAsteroidTiny();

                case AsteroidSize.SMALL:
                    return new SimpleAsteroidSmall();

                case AsteroidSize.MEDIUM:
                    return new SimpleAsteroidMedium();

                case AsteroidSize.BIG:
                    return new SimpleAsteroidBig();
            }

            return new SimpleAsteroidTiny();
            #pragma warning restore CS0618 // Type or member is obsolete

        }

        public override Asteroid CreateAsteroid()
        {
            return CreateAsteroid((AsteroidSize)ToolBox.Random.Next((int)AsteroidSize.TINY, (int)AsteroidSize.BIG + 1));
        }

        public override Bonus CreateBonus(Type type, BonusForceEnum bonusForce)
        {

            if (type == typeof(BonusShield))
            {
                return new BonusShield(bonusForce);
            }
            if (type == typeof(BonusSpeed))
            {
                return new BonusSpeed(bonusForce);
            }
            if (type == typeof(BonusHeal))
            {
                return new BonusHeal(bonusForce);
            }

            throw new UnknownTypeException($"{type} is not a bonus type");


        }

        public override Bonus CreateBonus()
        {
            var _force = (BonusForceEnum)ToolBox.Random.Next((int)BonusForceEnum.BRONZE, (int)BonusForceEnum.GOLD + 1);
            switch (ToolBox.Random.Next(0, 3))
            {
                case 0:
                    return CreateBonus(typeof(BonusShield), _force);
                    
                case 1:
                    return CreateBonus(typeof(BonusSpeed), _force);
                
                case 2:
                    return CreateBonus(typeof(BonusHeal), _force);
            }
            return null;

        }
    }
}
