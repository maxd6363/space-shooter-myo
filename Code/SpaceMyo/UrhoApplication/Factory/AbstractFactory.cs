﻿using System;
using UrhoApplication.Entity.Asteroid;
using UrhoApplication.Entity.Bonus;
using UrhoApplication.Utils;

namespace UrhoApplication.Factory
{
    public abstract class AbstractFactory
    {

        public abstract Asteroid CreateAsteroid(AsteroidSize size);
        public abstract Asteroid CreateAsteroid();


        public abstract Bonus CreateBonus(Type type ,BonusForceEnum bonusForce);
        public abstract Bonus CreateBonus();


    }
}
