﻿using System.Diagnostics;
using Urho;
using Urho.Urho2D;
using UrhoApplication.EntityGraphic;
using UrhoApplication.Utils;
using UrhoApplication.UIElement;
using UrhoApplication.Utils.I18n;
using UrhoApplication.Manager;
using System;

namespace UrhoApplication
{

    /// <summary>
    /// Entry point of the game Myastro
    /// </summary>
    public class Myastro : Application, IGame
    {
        /// <summary>
        /// Current instance of Myastro, following the design pattern of Urhosharp
        /// </summary>
        public static new Myastro Current { get => Application.Current as Myastro; }

        public Scene Scene { get; private set; }
        public CameraBackground Camera { get; private set; }
        public ViewportCustom Viewport { get; private set; }
        public MusicHandler MusicPlayer { get; private set; }
        public NavigationHandler Navigation { get; private set; }
        public ManagerAsteroids ManagerAsteroids { get; private set; }
        public ManagerMultiplayer ManagerMultiplayer { get; private set; }
        public ManagerSingleplayer ManagerSingleplayer { get; private set; }
        public ManagerBonus ManagerBonus { get; private set; }
        public ManagerMyos ManagerMyos { get; private set; }
        public GameSettings GameSettings { get; }
        public Cursor Cursor { get; private set; }
        public GUIFpsText GUIFps { get; private set; }
        public KeyboardShortcutHandler ShortcutHandler { get; private set; }
        
        
        /// <summary>
        /// Constructor of Myastro
        /// </summary>
        /// <param name="applicationOptions">specified options for launch</param>
        public Myastro(ApplicationOptions applicationOptions) : base(applicationOptions)
        {
            UnhandledException += Myastro_UnhandledException;
            Engine.PostRenderUpdate += Engine_PostRenderUpdate;
            ResourceCache.ResourceNotFound += ResourceCache_ResourceNotFound;

            GameSettings = new GameSettings
            {
                MusicGain = 0.07f,
                SoundGain = 0.17f
            };
            AppConfig.Current.Language = StringValues.EN_EN;
        }

        /// <summary>
        /// Callback when resource wasn't found
        /// </summary>
        /// <param name="obj"></param>
        private void ResourceCache_ResourceNotFound(Urho.Resources.ResourceNotFoundEventArgs obj)
        {
            throw new Exception($"Fail to load {obj.ResourceName}");
        }

        /// <summary>
        /// Post render event to draw physics on screen
        /// </summary>
        /// <param name="obj"></param>
        private void Engine_PostRenderUpdate(PostRenderUpdateEventArgs obj)
        {
            if (AppConfig.Current.DrawDebug)
            {
                Renderer.DrawDebugGeometry(true);
                var debugRendererComp = Scene.GetComponent<DebugRenderer>();
                Scene.GetComponent<PhysicsWorld2D>()?.DrawDebugGeometry(debugRendererComp, false);
            }
        }

        /// <summary>
        /// Every unhandled exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Myastro_UnhandledException(object sender, Urho.UnhandledExceptionEventArgs e)
        {
            Debug.WriteLine(e.Exception.StackTrace);
            e.Handled = true;
        }



        /// <summary>
        /// Start main method
        /// </summary>
        protected override void Start()
        {
            base.Start();
            Cursor = new Cursor();
            Scene = new Scene2DPhysics();
            Camera = new CameraBackground();
            Viewport = new ViewportCustom();
            GUIFps = new GUIFpsText();
            ShortcutHandler = new KeyboardShortcutHandler();
            MusicPlayer = new MusicHandler();
            Navigation = new NavigationHandler();
            ManagerAsteroids = new ManagerAsteroids();
            ManagerMultiplayer = new ManagerMultiplayer();
            ManagerSingleplayer = new ManagerSingleplayer();
            ManagerBonus = new ManagerBonus();
            ManagerMyos = new ManagerMyos();
        }
    }
}
