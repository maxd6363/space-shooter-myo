﻿using System.Collections.Generic;
using UrhoApplication.Entity;
using UrhoApplication.Entity.Ship;
using UrhoApplication.Input;

namespace UrhoApplication.Player
{
    public interface IPlayer : IDeletable
    {
        string Name { get; }
        float Score { get; set; }
        Ship ControlledShip { get; }
        AbstractInput MainInput { get; }
        


    }
}
