﻿using System;
using System.Diagnostics;
using UrhoApplication.Entity.Ship;
using UrhoApplication.EntityGraphic;
using UrhoApplication.Input;
using UrhoApplication.Input.Droid;
using UrhoApplication.Input.UWP.Gamepad;
using UrhoApplication.Input.UWP.Keyboard;
using UrhoApplication.Input.UWP.Myo;
using UrhoApplication.Manager;
using UrhoApplication.Utils;
using Urho;

namespace UrhoApplication.Player
{
    class SimplePlayer : IPlayer
    {
        public string Name { get; private set; }
        public float Score { get; set; }
        public Ship ControlledShip { get; private set; }
        public AbstractInput MainInput { get; private set; }
        public ImplementedInputs InputChoosed { get; }
        public int PlayerID { get; }

        public static uint PlayerCount { get; private set; } = 0;
        public static uint KeyboardInstanciateCount { get; private set; } = 0;
        public static uint GamepadInstanciateCount { get; private set; } = 0;
        public static uint MyoInstanciateCount { get; private set; } = 0;

        private TagText3D Tag { get; }



        /// <summary>
        /// Constructeur de simple player, gère son vaiseau, son controle avec input et son TagText3D
        /// Attention utilisation de l'introspection pour prendre le bon key mapping en fonction du player
        /// </summary>
        public SimplePlayer(ImplementedInputs inputChoosed = ImplementedInputs.UWP_KEYBOARD)
        {
            PlayerID = (int)PlayerCount;
            Name = AppConfig.Current.PlayerNames.Count == 0 ? $"Player {PlayerID}" : AppConfig.Current.PlayerNames[(int)PlayerID % AppConfig.Current.PlayerNames.Count];
            InputChoosed = inputChoosed;
            Score = 0;
            ControlledShip = new CommonShip(PlayerCount);
            Tag = new TagText3D(ControlledShip, Name, PlayerID);

            ControlledShip.ShipDied += ControlledShip_ShipDied;

            HandleInstanciationInput();

            PlayerCount++;



        }

        private void ControlledShip_ShipDied(object sender, EventArgs e)
        {
            Tag.Opacity = AppConfig.Current.ShipDeadOpacity;
        }

        private void HandleInstanciationInput()
        {
            switch (InputChoosed)
            {
                case ImplementedInputs.UWP_KEYBOARD:

                    MainInput = new InputKeyboard();
                    KeyboardInstanciateCount++;
                    break;


                case ImplementedInputs.UWP_GAMEPAD_URHO:
                    MainInput = new InputGamepadUrho(GamepadInstanciateCount);
                    GamepadInstanciateCount++;
                    break;

                case ImplementedInputs.UWP_MYO:
                    MainInput = (Application.Current as Myastro).ManagerMyos.AddMyo((int)MyoInstanciateCount);
                    MyoInstanciateCount++;
                    break;

                case ImplementedInputs.DROID_TOUCH:
                    MainInput = new InputTouch();
                    break;
            }

            if (MainInput != null)
                MainInput.InputPressed += Input_InputPressed;


        }



        private void Input_InputPressed(InputActionArgs obj)
        {
            ControlledShip.ThrusterForward(obj.ThrusterPower);
            ControlledShip.ShipRotation(obj.RotationVelocity);
            if (obj.Firing)
                ControlledShip.FireLaser();



        }


        public virtual void Delete()
        {
            MainInput.InputPressed -= Input_InputPressed;
            Tag.Delete();
            ControlledShip.Delete();
        }


        public static void ResetPlayerCount()
        {
            PlayerCount = 0;
            KeyboardInstanciateCount = 0;
            GamepadInstanciateCount = 0;
            MyoInstanciateCount = 0;
        }

    }
}
