﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho.Gui;
using UrhoApplication.Input;

namespace UrhoApplication.UIElement
{
    public class ButtonInputSelectorNoText : ButtonInputSelector
    {
        public ButtonInputSelectorNoText(float width, string text, HorizontalAlignment horizontalAlignment = HorizontalAlignment.Center, VerticalAlignment verticalAlignment = VerticalAlignment.Center, ButtonSize buttonSize = ButtonSize.SQUARE, ImplementedInputs input = ImplementedInputs.UWP_KEYBOARD) : base(width / 2, "", horizontalAlignment, verticalAlignment, buttonSize, input)
        {
            ID = text;
        }
    }
}
