﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Urho;
using UrhoApplication.UIElement.Menu;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement
{
    public class NavigationHandler
    {

        private MenuAbstract ActiveMenu
        {
            get => _activeMenu;
            set
            {
                if (_activeMenu == value) return;
                if (_activeMenu != null)
                    _activeMenu.UIClicked -= ActiveMenu_UIClicked;
                _activeMenu = value;
                if (_activeMenu != null)
                    _activeMenu.UIClicked += ActiveMenu_UIClicked;
            }
        }
        private MenuAbstract _activeMenu;

        /// <summary>
        /// Constructor
        /// </summary>
        public NavigationHandler()
        {
            ActiveMenu = new MainMenu(this);
        }


        /// <summary>
        /// The active menu has been clicked
        /// </summary>
        /// <param name="obj">UI Clicked</param>
        private void ActiveMenu_UIClicked(UICustom obj)
        {
            HandleAction(obj);

        }

        private void HandleAction(UICustom obj)
        {
            HandleActionMainMenu(obj);

            HandleActionMultiplayerGame(obj);
            HandleActionMultiplayer(obj);

            HandleActionSingleplayerGame(obj);
            HandleActionSingleplayer(obj);

        }

        private void HandleActionMultiplayerGame(UICustom obj)
        {
            if (obj.ID == AppConfig.Current.IDBackFromMultiplayerGame)
            {
                ActiveMenu.DestroyMenu();
                ActiveMenu = new MainMenu(this);
                Myastro.Current.ManagerMultiplayer.StopMultiplayer();
            }
            if (obj.ID == AppConfig.Current.IDSynchronMyoFromSettings)
            {
                (Application.Current as Myastro).ManagerMyos.SynchronizationMyos();
            }
        }

        private void HandleActionMultiplayer(UICustom obj)
        {
            if (obj.ID == AppConfig.Current.IDBackFromMultiplayerMenu)
            {
                ActiveMenu.DestroyMenu();
                ActiveMenu = new MainMenu(this);
            }
            if (obj.ID == AppConfig.Current.IDPlayer1)
            {
                if (ActiveMenu is MultiplayerMenu _menu)
                    _menu.Player1Button.CycleThroughInput();
            }
            if (obj.ID == AppConfig.Current.IDPlayer2)
            {
                if (ActiveMenu is MultiplayerMenu _menu)
                    _menu.Player2Button.CycleThroughInput();
            }
            if (obj.ID == AppConfig.Current.IDPlayer3)
            {
                if (ActiveMenu is MultiplayerMenu _menu)
                    _menu.Player3Button.CycleThroughInput();
            }
            if (obj.ID == AppConfig.Current.IDPlayer4)
            {
                if (ActiveMenu is MultiplayerMenu _menu)
                    _menu.Player4Button.CycleThroughInput();
            }

            if (obj.ID == AppConfig.Current.IDStartMultiplayer)
            {

                var _settings = Myastro.Current.GameSettings;

                if (ActiveMenu is MultiplayerMenu _menu)
                {
                    _settings.ResetMultiplayerInput();
                    _settings.AddMultiplayerInput(_menu.Player1Button.InputUsed);
                    _settings.AddMultiplayerInput(_menu.Player2Button.InputUsed);
                    _settings.AddMultiplayerInput(_menu.Player3Button.InputUsed);
                    _settings.AddMultiplayerInput(_menu.Player4Button.InputUsed);
                }

                ActiveMenu.DestroyMenu();
                ActiveMenu = new MultiplayerGameMenu(this);
                Myastro.Current.ManagerMultiplayer.StartMultiplayer();
            }
        }


        private void HandleActionSingleplayerGame(UICustom obj)
        {
            if (obj.ID == AppConfig.Current.IDBackFromSingleplayerGame)
            {
                ActiveMenu.DestroyMenu();
                ActiveMenu = new MainMenu(this);
                Myastro.Current.ManagerSingleplayer.StopSingleplayer();
            }
            if (obj.ID == AppConfig.Current.IDSynchronMyoFromSettings)
            {
                (Application.Current as Myastro).ManagerMyos.SynchronizationMyos();
            }
        }


        private void HandleActionSingleplayer(UICustom obj)
        {

            if (obj.ID == AppConfig.Current.IDBackFromSingleplayer)
            {
                ActiveMenu.DestroyMenu();
                ActiveMenu = new MainMenu(this);
            } 
            if (obj.ID == AppConfig.Current.IDPlayer)
            {
                if (ActiveMenu is SingleplayerMenu _menu)
                    _menu.PlayerButton.CycleThroughInput();
            }
                       
            if (obj.ID == AppConfig.Current.IDStartSingleplayer)
            {
                var _settings = Myastro.Current.GameSettings;

                if (ActiveMenu is SingleplayerMenu _menu)
                {
                    _settings.SetSingleplayerInput(_menu.PlayerButton.InputUsed);
                }

                ActiveMenu.DestroyMenu();
                ActiveMenu = new SingleplayerGameMenu(this);
                Myastro.Current.ManagerSingleplayer.StartSingleplayer();
            }
        }


        private void HandleActionMainMenu(UICustom obj)
        {
            if (ActiveMenu == null) return;
            if (obj.ID == AppConfig.Current.IDSingleplayer)
            {
                ActiveMenu.DestroyMenu();
                ActiveMenu = new SingleplayerMenu(this);
            }

            if (obj.ID == AppConfig.Current.IDMultiplayer)
            {
                ActiveMenu.DestroyMenu();
                ActiveMenu = new MultiplayerMenu(this);
            }

            if (obj.ID == AppConfig.Current.IDSettings)
            {
                ActiveMenu.DestroyMenu();
                ActiveMenu = new SettingsMenu(this);
            }

            if (obj.ID == AppConfig.Current.IDBackFromSettings)
            {
                ActiveMenu.DestroyMenu();
                ActiveMenu = new MainMenu(this);
            }
          
        }

    }
}
