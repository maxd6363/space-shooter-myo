﻿using System.Collections.Generic;
using System.Diagnostics;
using Urho;
using Urho.Gui;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement
{
    public class ButtonCustom : UICustom
    {

        public Text Text { get; }
        public bool EnabledButton
        {
            get => _enabled;
            set
            {
                if (value == _enabled) return;
                _enabled = value;
                if (_enabled) Enable();
                else Disable();
            }
        }

        private readonly float _width;
        private readonly ButtonSize _size;
        private bool _enabled;


        public ButtonCustom(float width, string text, HorizontalAlignment horizontalAlignment = HorizontalAlignment.Center, VerticalAlignment verticalAlignment = VerticalAlignment.Center, ButtonSize buttonSize = ButtonSize.LARGE) : base()
        {
            ID = text;

            _width = width;



            Text = new Text
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };
            Text.SetColor(AppConfig.Current.DefaultButtonColor);
            Text.SetFont(Application.Current.ResourceCache.GetFont(Assets.Fonts.KenvectorFuture), ToolBox.RelativeFontSize(AppConfig.Current.DefaultButtonFontSize));

            try
            {
                Text.Value = AppConfig.Current.Language[ID];
            }
            catch (KeyNotFoundException)
            {
                Text.Value = ID;
            }



            AddChild(Text);

            _size = buttonSize;

            switch (_size)
            {
                case ButtonSize.LARGE:
                    Texture = Application.Current.ResourceCache.GetTexture2D(Assets.Textures.ButtonBlue);
                    break;

                case ButtonSize.SQUARE:
                    Texture = Application.Current.ResourceCache.GetTexture2D(Assets.Textures.SmallButtonBlue);
                    break;

            }

            Size = ToolBox.AdaptedButtonSize(_width, _size);
            BlendMode = BlendMode.Alpha;
            HorizontalAlignment = horizontalAlignment;
            VerticalAlignment = verticalAlignment;
            MaxSize = AppConfig.Current.UIButtonMaxSize;
            EnabledButton = true;

            Application.Current.UI.Root.AddChild(this);




        }

        private void Enable()
        {
            Opacity = 0.9f;
        }

        private void Disable()
        {
            Opacity = 0.3f;
        }

    }
}
