﻿using System.Diagnostics;
using Urho;
using Urho.Gui;
using Urho.Urho2D;
using UrhoApplication.Input;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement
{
    public class ButtonInputSelector : ButtonCustom
    {

        public ImplementedInputs InputUsed { get; protected set; }
        private readonly BorderImage _icon;


        public ButtonInputSelector(float width, string text, HorizontalAlignment horizontalAlignment = HorizontalAlignment.Center, VerticalAlignment verticalAlignment = VerticalAlignment.Center, ButtonSize buttonSize = ButtonSize.LARGE, ImplementedInputs input = ImplementedInputs.UWP_KEYBOARD) : base(width * 1.1f, text, horizontalAlignment, verticalAlignment, buttonSize)
        {
            InputUsed = input;
            Text.SetFontSize(Text.FontSize * 0.8f);
            _icon = new BorderImage
            {
                Position = AppConfig.Current.UIIconPosition,
                Size = ToolBox.RelativeIconSize(),
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Left
            };


            ChangeIcon();



            AddChild(_icon);

        }


        private void ChangeIcon()
        {
            Texture2D texture;

            switch (InputUsed)
            {

                case ImplementedInputs.UWP_KEYBOARD:
                    texture = Application.Current.ResourceCache.GetTexture2D(Assets.Textures.IconInputKeyboard);
                    break;
                case ImplementedInputs.UWP_GAMEPAD_URHO:
                    texture = Application.Current.ResourceCache.GetTexture2D(Assets.Textures.IconInputGamepad);
                    break;
                case ImplementedInputs.UWP_MYO:
                    texture = Application.Current.ResourceCache.GetTexture2D(Assets.Textures.IconInputMyo);
                    break;
                case ImplementedInputs.DROID_TOUCH:
                    texture = Application.Current.ResourceCache.GetTexture2D(Assets.Textures.IconInputMouse);
                    break;
                default:
                    texture = Application.Current.ResourceCache.GetTexture2D(Assets.Textures.IconInputNothing);
                    break;

            }
            _icon.Texture = texture;
        }


        public void CycleThroughInput()
        {
            InputUsed = (ImplementedInputs)(((int)(InputUsed + 1)) % 5);
            ChangeIcon();
            Debug.WriteLine("CLICKED");
        }

   




    }
}
