﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho.Gui;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement.Menu
{
    public class MultiplayerMenu : MenuAbstract
    {
        public ButtonInputSelector Player1Button { get; }
        public ButtonInputSelector Player2Button { get; }
        public ButtonInputSelector Player3Button { get; }
        public ButtonInputSelector Player4Button { get; }
        private readonly ButtonCustom _startMultiplayerButton;
        private readonly ButtonCustom _backButton;


        public MultiplayerMenu(NavigationHandler navigation) : base(navigation)
        {

            Player1Button = new ButtonInputSelector(0.3f, AppConfig.Current.IDPlayer1, HorizontalAlignment.Center, VerticalAlignment.Center,input: Input.ImplementedInputs.UWP_GAMEPAD_URHO)
            {
                Position = ToolBox.RelativePosition(-0.2f, -0.05f)
            };
            Player2Button = new ButtonInputSelector(0.3f, AppConfig.Current.IDPlayer2, HorizontalAlignment.Center, VerticalAlignment.Center, input: Input.ImplementedInputs.UWP_GAMEPAD_URHO)
            {
                Position = ToolBox.RelativePosition(0.2f, -0.05f)
            };
            Player3Button = new ButtonInputSelector(0.3f, AppConfig.Current.IDPlayer3, HorizontalAlignment.Center, VerticalAlignment.Center, input: Input.ImplementedInputs.NONE)
            {
                Position = ToolBox.RelativePosition(-0.2f, 0.05f)
            };
            Player4Button = new ButtonInputSelector(0.3f, AppConfig.Current.IDPlayer4, HorizontalAlignment.Center, VerticalAlignment.Center, input: Input.ImplementedInputs.NONE)
            {
                Position = ToolBox.RelativePosition(0.2f, 0.05f)
            };

            _startMultiplayerButton = new ButtonCustom(0.3f, AppConfig.Current.IDStartMultiplayer, HorizontalAlignment.Center, VerticalAlignment.Bottom)
            {
                Position = ToolBox.RelativePosition(0.0f, -0.02f)
            };

            _backButton = new ButtonCustom(AppConfig.Current.UIBackButtonSize, AppConfig.Current.IDBackFromMultiplayerMenu, HorizontalAlignment.Right, VerticalAlignment.Bottom)
            {
                Position = ToolBox.RelativePosition(AppConfig.Current.UIBackButtonMargin, AppConfig.Current.UIBackButtonMargin)
            };

            Elements.Add(_backButton);

            Elements.Add(Player1Button);
            Elements.Add(Player2Button);
            Elements.Add(Player3Button);
            Elements.Add(Player4Button);
            Elements.Add(_startMultiplayerButton);
            Elements.Add(_backButton);


        }

    }
}
