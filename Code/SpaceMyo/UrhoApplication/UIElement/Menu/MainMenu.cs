﻿using System.Diagnostics;
using Urho;
using Urho.Gui;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement.Menu
{
    public class MainMenu : MenuAbstract
    {

        private readonly Text _myastro;
        private readonly ButtonCustom _singleplayerButton;
        private readonly ButtonCustom _multiplayerButton;
        private readonly ButtonCustom _settingsButton;


        public MainMenu(NavigationHandler navigation) : base(navigation)
        {

            _singleplayerButton = new ButtonCustom(AppConfig.Current.DefaultButtonWidth, AppConfig.Current.IDSingleplayer)
            {
                Position = ToolBox.RelativePosition(0, -0.05f)
            };

            _multiplayerButton = new ButtonCustom(AppConfig.Current.DefaultButtonWidth, AppConfig.Current.IDMultiplayer)
            {
                Position = ToolBox.RelativePosition(0, 0.05f),
            };


            _settingsButton = new ButtonCustom(AppConfig.Current.DefaultButtonWidth, AppConfig.Current.IDSettings)
            {
                Position = ToolBox.RelativePosition(0, 0.15f)
            };


            _myastro = new Text()
            {
                Value = "MYASTRO",
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Position = ToolBox.RelativePosition(0, -0.15f),
                TextAlignment = HorizontalAlignment.Center,
                EffectShadowOffset = new IntVector2(7, 7),
                TextEffect = TextEffect.Shadow
            };

            _myastro.SetFont(Assets.Fonts.KenvectorFuture, ToolBox.RelativeFontSize(120));
            _myastro.SetColor(Color.White);
            Application.Current.UI.Root.AddChild(_myastro);




            Elements.Add(_singleplayerButton);
            Elements.Add(_multiplayerButton);
            Elements.Add(_settingsButton);

        }


        public override void DestroyMenu()
        {
            base.DestroyMenu();
            Application.Current.UI.Root.RemoveChild(_myastro);
        }

    }
}
