﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho.Gui;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement.Menu
{
    public class MultiplayerGameMenu : MenuAbstract
    {



        private readonly ButtonCustom _synchronMyoButton;
        private readonly ButtonCustom _backButton;


        public MultiplayerGameMenu(NavigationHandler navigation) : base(navigation)
        {
            _backButton = new ButtonCustom(AppConfig.Current.UIBackButtonSize, AppConfig.Current.IDBackFromMultiplayerGame, HorizontalAlignment.Right, VerticalAlignment.Bottom)
            {
                Position = ToolBox.RelativePosition(AppConfig.Current.UIBackButtonMargin, AppConfig.Current.UIBackButtonMargin)
            };
            _synchronMyoButton = new ButtonCustom(AppConfig.Current.UISynchronMyoButtonSize, AppConfig.Current.IDSynchronMyoFromSettings, HorizontalAlignment.Left, VerticalAlignment.Bottom) 
            {
                Position = ToolBox.RelativePosition(AppConfig.Current.UISynchronMyoButtonMargin, AppConfig.Current.UIBackButtonMargin)
            };


            Elements.Add(_backButton);
            Elements.Add(_synchronMyoButton);
        }

    }
}
