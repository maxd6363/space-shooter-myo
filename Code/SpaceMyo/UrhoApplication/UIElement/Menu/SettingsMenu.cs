﻿using Urho.Gui;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement.Menu
{
    class SettingsMenu : MenuAbstract
    {

        private readonly ButtonCustom _backButton;


        public SettingsMenu(NavigationHandler navigation) : base(navigation)
        {
            _backButton = new ButtonCustom(AppConfig.Current.UIBackButtonSize, AppConfig.Current.IDBackFromSettings, HorizontalAlignment.Right, VerticalAlignment.Bottom)
            {
                Position = ToolBox.RelativePosition(AppConfig.Current.UIBackButtonMargin, AppConfig.Current.UIBackButtonMargin)
            };

            Elements.Add(_backButton);
        }

    }
}
