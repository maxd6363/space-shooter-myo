﻿using Urho;
using Urho.Gui;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement.Menu
{
    class SingleplayerGameMenu : MenuAbstract
    {


        private readonly ButtonCustom _synchronMyoButton;
        private readonly ButtonCustom _backButton;
        private readonly Text Score;
        private readonly Myastro App;


        public SingleplayerGameMenu(NavigationHandler navigation) : base(navigation)
        {
            App = Application.Current as Myastro;
            _backButton = new ButtonCustom(AppConfig.Current.UIBackButtonSize, AppConfig.Current.IDBackFromSingleplayerGame, HorizontalAlignment.Right, VerticalAlignment.Bottom)
            {
                Position = ToolBox.RelativePosition(AppConfig.Current.UIBackButtonMargin, AppConfig.Current.UIBackButtonMargin)
            };

            _synchronMyoButton = new ButtonCustom(AppConfig.Current.UISynchronMyoButtonSize, AppConfig.Current.IDSynchronMyoFromSettings, HorizontalAlignment.Left, VerticalAlignment.Bottom)
            {
                Position = ToolBox.RelativePosition(AppConfig.Current.UISynchronMyoButtonMargin, AppConfig.Current.UIBackButtonMargin)
            };

            Elements.Add(_backButton);
            Elements.Add(_synchronMyoButton);

            Score = new Text
            {
                Position = new IntVector2(-20, 20),
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Right
            };

            Score.SetColor(Color.White);
            Score.SetFont(Application.Current.ResourceCache.GetFont(Assets.Fonts.AnonymousPro), AppConfig.Current.DefaultButtonFontSize);
            Application.Current.UI.Root.AddChild(Score);

            Application.Current.Update += Update;
        }

        private void Update(UpdateEventArgs obj)
        {
            Score.Value = $"Score : {App.ManagerSingleplayer.Player.Score:F0}";
        }

        public override void DestroyMenu()
        {
            base.DestroyMenu();
            Application.Current.Update -= Update;
            Application.Current.UI.Root.RemoveChild(Score);

        }
    }
}
