﻿using System.Diagnostics;
using Urho.Gui;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement.Menu
{
    class SingleplayerMenu : MenuAbstract
    {
        public ButtonInputSelector PlayerButton { get; }

        private readonly ButtonCustom _backButton;
        private readonly ButtonCustom _startMultiplayerButton;


        public SingleplayerMenu(NavigationHandler navigation) : base(navigation)
        {



            int _idLevel = 0;
            float _XMargin;
            float _YMargin = 0.1f;

            for (int rows = 0; rows < 3; rows++)
            {
                _XMargin = 0.1f;
                var _verticalAlignment = (VerticalAlignment)rows;
                for (int cols = 0; cols < 3; cols++)
                {


                    var _horizontalAlignment = (HorizontalAlignment)cols;
                    var _levelI = new ButtonCustom(0.1f, AppConfig.Current.IDLevels[_idLevel], _horizontalAlignment, _verticalAlignment, ButtonSize.SQUARE)
                    {
                        Position = ToolBox.RelativePosition(_XMargin, _YMargin)
                    };
                    Elements.Add(_levelI);
                    _idLevel++;
                    _XMargin -= 0.1f;

                }
                _YMargin -= 0.1f;
            }


            PlayerButton = new ButtonInputSelectorNoText(0.15f, AppConfig.Current.IDPlayer, HorizontalAlignment.Left, VerticalAlignment.Bottom, input: AppConfig.Current.DefaultInputSelected)
            {
                Position = ToolBox.RelativePosition(0.01f, -0.01f)
            };

            _startMultiplayerButton = new ButtonCustom(0.3f, AppConfig.Current.IDStartSingleplayer, HorizontalAlignment.Center, VerticalAlignment.Bottom)
            {
                Position = ToolBox.RelativePosition(0.0f, -0.02f)
            };

            _backButton = new ButtonCustom(AppConfig.Current.UIBackButtonSize, AppConfig.Current.IDBackFromSingleplayer, HorizontalAlignment.Right, VerticalAlignment.Bottom)
            {
                Position = ToolBox.RelativePosition(AppConfig.Current.UIBackButtonMargin, AppConfig.Current.UIBackButtonMargin)
            };


            Elements.Add(PlayerButton);
            Elements.Add(_startMultiplayerButton);
            Elements.Add(_backButton);


        }

    }
}
