﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Urho;
using Urho.Gui;
using UrhoApplication.Utils;

namespace UrhoApplication.UIElement.Menu
{
    public abstract class MenuAbstract
    {

        public event Action<UICustom> UIClicked
        {
            add => _UIClicked += value;
            remove => _UIClicked -= value;
        }

        protected IList<UICustom> Elements { get; }
        protected BorderImage Background { get; }
        protected NavigationHandler Navigation { get; }

        protected Action<UICustom> _UIClicked;



        public MenuAbstract(NavigationHandler navigation)
        {
            Navigation = navigation;

            Elements = new List<UICustom>();
            Background = new BorderImage
            {
                Opacity = 0.2f
            };

            Application.Current.UI.UIMouseClick += UI_UIMouseClick;
           
        }

        protected virtual void UI_UIMouseClick(UIMouseClickEventArgs obj)
        {
            try
            {
                foreach (UICustom e in Elements)
                {
                    if (ToolBox.IsPosInRect(obj.X, obj.Y, e.CombinedScreenRect))
                    {
                        _UIClicked?.Invoke(e);
                        return;
                    }

                }

            }
            catch (InvalidOperationException)
            {

            }
        }


        public virtual void Hide()
        {
            foreach (UICustom e in Elements)
                e.Visible = false;
        }

        public virtual void Show()
        {
            foreach (UICustom e in Elements)
                e.Visible = true;
        }


        public virtual void DestroyMenu()
        {
            foreach (UICustom e in Elements)
                Application.Current.UI.Root.RemoveChild(e);
        }


   


    }
}
