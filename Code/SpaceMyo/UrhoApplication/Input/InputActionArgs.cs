﻿namespace UrhoApplication.Input
{
    public struct InputActionArgs
    {
        public float ThrusterPower { get; }
        public float RotationVelocity { get; }
        public bool Firing { get; }

        public InputActionArgs(float thrusterPower, float degreeLook, bool firing)
        {
            ThrusterPower = thrusterPower;
            RotationVelocity = degreeLook;
            Firing = firing;
        }

        public override string ToString()
        {
            return $"Power : {ThrusterPower:F3} | Degree : {RotationVelocity:F3}° | Firing {Firing}";
        }
    }
}