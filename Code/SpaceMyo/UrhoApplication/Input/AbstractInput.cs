﻿using System;
using Urho;

namespace UrhoApplication.Input
{
    public abstract class AbstractInput
    {
        public bool IsConnected { get; protected set; }

        public event Action<AbstractInput> TookOver
        {
            add => _tookOver += value;
            remove => _tookOver -= value;
        }

        public event Action<InputActionArgs> InputPressed
        {
            add => _inputPressed += value;
            remove => _inputPressed -= value;
        }

        
        protected Action<AbstractInput> _tookOver;
        protected Action<InputActionArgs> _inputPressed;

        protected float _thrusterPower;
        protected float _rotationVelocity;
        protected bool _firing;



        public AbstractInput()
        {
            IsConnected = false;
            Application.Current.Update += Update;
        }

        protected virtual void Update(UpdateEventArgs obj)
        {
        }
    }
}
