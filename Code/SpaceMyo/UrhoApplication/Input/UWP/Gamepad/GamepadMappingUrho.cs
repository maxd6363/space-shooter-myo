﻿namespace UrhoApplication.Input.UWP.Gamepad
{
    public enum GamepadMappingUrho
    {
        A = 0,
        B = 1,
        X = 2,
        Y = 3,
        SELECT = 4,
        XBOX = 5,
        START = 6,
        L3 = 7,
        R3 = 8,
        LB = 9,
        RB = 10,
        UP = 11,
        DOWN = 12,
        LEFT = 13,
        RIGHT = 14
    }
}
