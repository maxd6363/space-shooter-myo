﻿using System;
using System.Diagnostics;
using Urho;
using UrhoApplication.Utils;

namespace UrhoApplication.Input.UWP.Gamepad
{
    public class InputGamepadUrho : AbstractInput
    {
        private Urho.Input Input { get => Application.Current.Input; }



        private JoystickState GamepadUrho;


        public InputGamepadUrho(uint gamepadIndex)
        {
            
            try
            {

              

                if (Input.TryGetJoystickState(gamepadIndex, out GamepadUrho))
                {
                    IsConnected = true;
                }


            }
            catch (IndexOutOfRangeException e)
            {
                Debug.WriteLine(e);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Debug.WriteLine(e);
            }
            Debug.WriteLine(IsConnected);

        }

       /// <summary>
       /// Called each frame
       /// </summary>
       /// <param name="obj"></param>
        protected override void Update(UpdateEventArgs obj)
        {
            if (!IsConnected) return;
            HandleTakeOver();


            bool _fire = HandleFiring();
            bool _power = HandleThrusterPower();
            bool _dir = HandleDirection();

            if (_fire || _power || _dir)
            {
                _inputPressed?.Invoke(new InputActionArgs(_thrusterPower, _rotationVelocity, _firing));
            }

        }

        /// <summary>
        /// Detect when a key is pressed
        /// </summary>
        private void HandleTakeOver()
        {
            if (GamepadUrho.GetButtonDown((int)GamepadMappingUrho.A) == 1 || GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_X) != 0 || GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_Y) != 0 || GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_TRIGGER) != 0 || GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.RIGHT_TRIGGER) != 0)
            {
                _tookOver?.Invoke(this);
            }


        }

        private bool HandleFiring()
        {

            if (GamepadUrho.GetButtonDown((int)GamepadMappingUrho.A) == 1)
            {

                _firing = true;
                return true;
            }
            _firing = false;
            return false;

        }

        private bool HandleThrusterPower()
        {
            float _boost = AppConfig.Current.ShipThrusterBaseSpeed;
            if (GamepadUrho.GetButtonDown((int)GamepadMappingUrho.X) == 1)
            {
                _boost = AppConfig.Current.ShipThrusterBoostedSpeed;
            }



            if (GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.RIGHT_TRIGGER) >= AppConfig.Current.GamepadMarginAnalogic)
            {
                _thrusterPower = _boost * (float)(GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.RIGHT_TRIGGER) > 1 ? 1 : GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.RIGHT_TRIGGER));
                return true;
            }

            if (GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_Y) >= AppConfig.Current.GamepadMarginAnalogic)
            {
                _thrusterPower = _boost * (float)(GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_Y) > 1 ? 1 : GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_Y));
                return true;
            }

            if (GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_TRIGGER) >= AppConfig.Current.GamepadMarginAnalogic)
            {
                _thrusterPower = AppConfig.Current.ShipThrusterReverseSpeed * (float)(GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_TRIGGER) > 1 ? 1 : GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_TRIGGER));
                return true;
            }

            if (GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_Y) <= AppConfig.Current.GamepadMarginAnalogic)
            {
                _thrusterPower = AppConfig.Current.ShipThrusterReverseSpeed * (float)(-GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_Y) > 1 ? 1 : -GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_Y));
                return true;
            }

            _thrusterPower = AppConfig.Current.ShipThrusterZeroSpeed;
            return false;

        }


        private bool HandleDirection()
        {



            if (GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_X) >= AppConfig.Current.GamepadMarginAnalogic)
            {
                _rotationVelocity = AppConfig.Current.EntityRotationRightSpeed * (float)GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_X);

                return true;
            }

            if (GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_X) <= AppConfig.Current.GamepadMarginAnalogic)
            {
                _rotationVelocity = AppConfig.Current.EntityRotationLeftSpeed * (float)-GamepadUrho.GetAxisPosition((int)GamepadJoystickMappingUrho.LEFT_X);
                return true;
            }


            _rotationVelocity = AppConfig.Current.EntityRotationNoSpeed;
            return true;
        }


        public override string ToString()
        {
            return $"{typeof(InputGamepadUrho)}";
        }





    }
}
