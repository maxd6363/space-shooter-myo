﻿using Urho;

namespace UrhoApplication.Input.UWP.Keyboard
{
    class KeyMappingPlayer1 : KeyMapping
    {
        public override Key Forward { get; protected set; } = Key.Z;
        public override Key Backward { get; protected set; } = Key.S;
        public override Key Left { get; protected set; } = Key.Q;
        public override Key Right { get; protected set; } = Key.D;
        public override Key Fire { get; protected set; } = Key.Space;
        public override Key Boost { get; protected set; } = Key.LeftShift;
    }
}

