﻿using Urho;

namespace UrhoApplication.Input.UWP.Keyboard
{
    class KeyMappingPlayer3 : KeyMapping
    {
        public override Key Forward { get; protected set; } = Key.O;
        public override Key Backward { get; protected set; } = Key.L;
        public override Key Left { get; protected set; } = Key.K;
        public override Key Right { get; protected set; } = Key.M;
        public override Key Fire { get; protected set; } = Key.I;
        public override Key Boost { get; protected set; } = Key.P;


    }
}
