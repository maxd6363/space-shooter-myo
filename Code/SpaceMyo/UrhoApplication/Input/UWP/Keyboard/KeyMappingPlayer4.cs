﻿using Urho;

namespace UrhoApplication.Input.UWP.Keyboard
{
    class KeyMappingPlayer4 : KeyMapping
    {
        public override Key Forward { get; protected set; } = Key.KP_8;
        public override Key Backward { get; protected set; } = Key.KP_5;
        public override Key Left { get; protected set; } = Key.KP_4;
        public override Key Right { get; protected set; } = Key.KP_6;
        public override Key Fire { get; protected set; } = Key.Kp_ENTER;
        public override Key Boost { get; protected set; } = Key.KP_0;


    }
}
