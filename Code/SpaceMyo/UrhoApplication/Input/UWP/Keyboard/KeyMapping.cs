﻿using System.Collections.Generic;
using Urho;

namespace UrhoApplication.Input.UWP.Keyboard
{
    public class KeyMapping
    {
        public virtual Key Forward { get; protected set; }
        public virtual Key Backward { get; protected set; }
        public virtual Key Left { get; protected set; }
        public virtual Key Right { get; protected set; }
        public virtual Key Fire { get; protected set; }
        public virtual Key Boost { get; protected set; }

        public override string ToString()
        {
            return $"Forward : {Forward} | Backward : {Backward} | Left : {Left} | Right : {Right} | Fire : {Fire} | Boost : {Boost}";
        }
    }
}
