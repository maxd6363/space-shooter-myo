﻿using Urho;

namespace UrhoApplication.Input.UWP.Keyboard
{
    class KeyMappingPlayer2 : KeyMapping
    {
        public override Key Forward { get; protected set; } = Key.Up;
        public override Key Backward { get; protected set; } = Key.Down;
        public override Key Left { get; protected set; } = Key.Left;
        public override Key Right { get; protected set; } = Key.Right;
        public override Key Fire { get; protected set; } = Key.RightCtrl;
        public override Key Boost { get; protected set; } = Key.RightShift;

    }
}

