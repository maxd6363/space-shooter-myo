﻿using System;
using System.Diagnostics;
using System.Reflection;
using Urho;
using UrhoApplication.Player;
using UrhoApplication.Utils;

namespace UrhoApplication.Input.UWP.Keyboard
{
    public class InputKeyboard : AbstractInput
    {
        private KeyMapping KeyMapping { get; }

        public InputKeyboard()
        {
            try
            {
                Assembly assembly = typeof(KeyMapping).Assembly;
                var _mapping = assembly.CreateInstance($"UrhoApplication.Input.UWP.Keyboard.KeyMappingPlayer{SimplePlayer.KeyboardInstanciateCount + 1}") as KeyMapping;
                KeyMapping = _mapping ?? throw new NotImplementedException($"Mapping for player ");

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                Debug.WriteLine("Default Input Set");
                KeyMapping = new KeyMappingPlayer1();
            }
            IsConnected = true;
        }

        protected override void Update(UpdateEventArgs obj)
        {
            if (!IsConnected) return;
            HandleTakeOver();


            bool _fire = HandleFiring();
            bool _power = HandleThrusterPower();
            bool _dir = HandleDirection();

            if (_fire || _power || _dir)
            {
                _inputPressed?.Invoke(new InputActionArgs(_thrusterPower, _rotationVelocity, _firing));
            }

        }

        /// <summary>
        /// Detecte quand une des touches du clavier est pressé
        /// Utilise l'introspection pour écouter toutes les touches utilisables définit dans la class KeyMapping
        /// </summary>
        private void HandleTakeOver()
        {
            var _keysField = typeof(KeyMapping).GetFields();
            foreach (FieldInfo fieldInfo in _keysField)
            {
                var _key = (Key)fieldInfo.GetValue(null);
                if (ToolBox.IsKeyDown(_key, true))
                {
                    _tookOver?.Invoke(this);
                    return;
                }
            }


        }

        private bool HandleFiring()
        {

            if (ToolBox.IsKeyDown(KeyMapping.Fire, true))
            {

                _firing = true;
                return true;
            }
            _firing = false;
            return false;

        }

        private bool HandleThrusterPower()
        {
            if (ToolBox.IsKeyDown(KeyMapping.Forward, true))
            {
                if(ToolBox.IsKeyDown(KeyMapping.Boost, true))
                {
                    _thrusterPower = AppConfig.Current.ShipThrusterBoostedSpeed;
                    return true;
                }
                _thrusterPower = AppConfig.Current.ShipThrusterBaseSpeed;
                return true;
            }

            if (ToolBox.IsKeyDown(KeyMapping.Backward, true))
            {
                _thrusterPower = AppConfig.Current.ShipThrusterReverseSpeed;
                return true;
            }

            _thrusterPower = AppConfig.Current.ShipThrusterZeroSpeed;
            return false;

        }


        private bool HandleDirection()
        {



            if (ToolBox.IsKeyDown(KeyMapping.Left, true))
            {
                _rotationVelocity = AppConfig.Current.EntityRotationLeftSpeed;
                return true;
            }

            if (ToolBox.IsKeyDown(KeyMapping.Right, true))
            {
                _rotationVelocity = AppConfig.Current.EntityRotationRightSpeed;
                return true;
            }


            _rotationVelocity = AppConfig.Current.EntityRotationNoSpeed;
            return true;
        }


        public override string ToString()
        {
            return $"{typeof(InputKeyboard)}";
        }

    }
}
