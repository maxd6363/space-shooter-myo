﻿using MyoLib;
using MyoSharp.Poses;
using MyoSharp.Device;
using Urho;
using UrhoApplication.Utils;


namespace UrhoApplication.Input.UWP.Myo
{
    public class MyoInput : AbstractMyo
    {
        /// <summary>
        /// Manager from MyoLib
        /// </summary>
        private MyoManager manager;

        /// <summary>
        /// player pose
        /// </summary>
        private Pose actualPose;

        /// <summary>
        /// power
        /// </summary>
        private float power;

        /// <summary>
        /// direction
        /// </summary>
        private float direction;

        /// <summary>
        ///  value for myo recalibration (power)
        /// </summary>
        private float adjustPower;

        /// <summary>
        /// value for myo recalibration (direction)
        /// </summary>
        private float adjustDirection;

        /// <summary>
        /// Myo id
        /// </summary>
        private int myId;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="myoId">Myo id</param>
        public MyoInput(int myoId)
        {

            manager = new MyoManager();
            manager.Init();
            manager.MyoConnected += Connected;
            manager.PoseChanged += Poses;
            manager.MyoLocked += LockUnlock;
            manager.StartListening();
            adjustDirection = 0;
            adjustPower = 0;
            IsConnected = true;
            myId = myoId;

        }

        /// <summary>
        /// Forces the myo's unlock
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Myo event</param>
        private void LockUnlock(object sender, MyoEventArgs e)
        {
            manager.UnlockAll(UnlockType.Hold);
        }

        /// <summary>
        /// Player's pose recovery
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Myo poses event</param>
        private void Poses(object sender, PoseEventArgs e)
        {
            actualPose = e.Pose;
        }

        /// <summary>
        /// Arm orientation datas recovery
        /// </summary>
        /// <param name="e">Myo orientation event</param>
        private void Orientation(OrientationDataEventArgs e)
        {
            power = adjustPower + (float)e.Pitch;
            direction = adjustDirection + (float)e.Yaw;
        }

        /// <summary>
        /// Connecction with the myo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Myo event</param>
        private void Connected(object sender, MyoEventArgs e)
        {
            IsConnected = true;
            manager.SubscribeToOrientationData(myId, (source, args) => Orientation(args));

        }

        /// <summary>
        /// Game loop
        /// </summary>
        /// <param name="obj">Update event</param>
        protected override void Update(UpdateEventArgs obj)
        {
            if (!IsConnected) return;

            bool _fire = MyoFiring();
            bool _power = MyoThrusterPower();
            bool _dir = MyoDirection();

            if (_fire || _power || _dir)
            {
                _inputPressed?.Invoke(new InputActionArgs(_thrusterPower, _rotationVelocity, _firing));
            }

        }

        /// <summary>
        /// Shoot misiles's action
        /// /// </summary>
        /// <returns>if ship need to shoot</returns>
        private bool MyoFiring()
        {
            if (actualPose == Pose.DoubleTap || actualPose == Pose.Fist)
            {
                _firing = true;
                return true;
            }
            _firing = false;
            return false;
        }

        /// <summary>
        /// Go to advance or to go back
        /// </summary>
        /// <returns>if ship need to move or not</returns>
        private bool MyoThrusterPower()
        {

            if (power < -AppConfig.Current.MyoMarginAnalogic)
            {
                _thrusterPower = AppConfig.Current.ShipThrusterBaseSpeed * -power;
                return true;
            }
            if (power > AppConfig.Current.MyoMarginAnalogic)
            {
                _thrusterPower = AppConfig.Current.ShipThrusterReverseSpeed * power;
                return true;
            }
            _thrusterPower = AppConfig.Current.ShipThrusterZeroSpeed;
            return false;

        }

        /// <summary>
        /// Go to left ou right
        /// </summary>
        /// <returns>if ship need to move or not</returns>
        private bool MyoDirection()
        {
            if (direction > AppConfig.Current.MyoMarginAnalogic)
            {
                _rotationVelocity = AppConfig.Current.EntityRotationLeftSpeed * direction;
                return true;
            }
            if (direction < -AppConfig.Current.MyoMarginAnalogic)
            {
                _rotationVelocity = AppConfig.Current.EntityRotationRightSpeed * -direction;
                return true;
            }
            _rotationVelocity = AppConfig.Current.EntityRotationNoSpeed;
            return false;
        }

        /// <summary>
        /// Myo's recalibration 
        /// </summary>
        public void SynchronizeMyMyo()
        {
            adjustPower = -power;
            adjustDirection = -direction;
        }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns>variable power</returns>
        public override string ToString()
        {
            return power.ToString();
        }

    }
}
