﻿using System;
using Urho;
using UrhoApplication.Utils;

namespace UrhoApplication.Input.Droid
{
    class InputTouch : AbstractInput
    {
        private readonly int[] _x;
        private readonly int[] _y;
        private readonly int numberOfInputs = 10;

        public InputTouch()
        {
            IsConnected = true;


            _x = new int[numberOfInputs];
            _y = new int[numberOfInputs];

            for (int i = 0; i < numberOfInputs; i++)
            {
                _x[i] = 0;
                _y[i] = 0;
            }

        }





        protected override void Update(UpdateEventArgs obj)
        {
            if (!IsConnected) return;

            for (uint i = 0; i < numberOfInputs; i++)
            {

                try { _x[i] = Application.Current.Input.GetTouch(i).Position.X; }
                catch (NullReferenceException) { _x[i] = 0; }
                try { _y[i] = Application.Current.Input.GetTouch(i).Position.Y; }
                catch (NullReferenceException) { _y[i] = 0; }
            }


            bool _fire = HandleFiring();
            bool _power = HandleThrusterPower();
            bool _dir = HandleDirection();

            if (_fire || _power || _dir)
            {
                _inputPressed?.Invoke(new InputActionArgs(_thrusterPower, _rotationVelocity, _firing));
            }

        }

        /// <summary>
        /// Detecte quand une des touches du clavier est pressé
        /// Utilise l'introspection pour écouter toutes les touches utilisables définit dans la class KeyMapping
        /// </summary>


        private bool HandleFiring()
        {

            for (int i = 0; i < numberOfInputs; i++)
            {
                if (_x[i] > 700 && _x[i] < 1400 && _y[i] > 0 && _y[i] < 500)
                {
                    _firing = true;
                    return true;
                }
            }

            _firing = false;
            return false;
        }

        private bool HandleThrusterPower()
        {
            for (int i = 0; i < numberOfInputs; i++)
            {
                if (_x[i] > 700 && _x[i] < 1400 &&  _y[i] > 600 && _y[i] < 1080)
                {
                    _thrusterPower = AppConfig.Current.ShipThrusterBaseSpeed;
                    return true;
                }
            }
            _thrusterPower = AppConfig.Current.ShipThrusterZeroSpeed;
            return false;

        }


        private bool HandleDirection()
        {

            for (int i = 0; i < numberOfInputs; i++)
            {

                if (_x[i] > 100 && _x[i] < 600)
                {
                    _rotationVelocity = AppConfig.Current.EntityRotationLeftSpeed;
                    return true;
                }

                if (_x[i] > 1400 && _x[i] < 2100)
                {
                    _rotationVelocity = AppConfig.Current.EntityRotationRightSpeed;
                    return true;
                }
            }

            _rotationVelocity = AppConfig.Current.EntityRotationNoSpeed;
            return false;
        }


        public override string ToString()
        {
            return $"{typeof(InputTouch)}";
        }

    }
}
