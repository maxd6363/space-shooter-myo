﻿
using Urho;

namespace Myastro.Desktop
{
    public class Program
    {
        private static UrhoApplication.Myastro app;

        static void Main(string[] args)
        {
            var option = new ApplicationOptions("data")
            {
                LimitFps = true,
                WindowedMode = true,
                ResizableWindow = true,
                Width = 2560/2,
                Height = 1440/2,
                TouchEmulation = true
            };



            app = new UrhoApplication.Myastro(option);

            app.Run();

        }

    }
}
