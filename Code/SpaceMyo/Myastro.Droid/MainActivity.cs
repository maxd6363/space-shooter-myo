﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Urho.Droid;


namespace Myastro.Droid
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true, ScreenOrientation = ScreenOrientation.Landscape),]
    public class MainActivity : AppCompatActivity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            
            var mLayout = new AbsoluteLayout(this);
            var surface = UrhoSurface.CreateSurface(this);
            
            mLayout.AddView(surface);
            SetContentView(mLayout);
            var _appOptions = new Urho.ApplicationOptions("Data")
            {
                LimitFps = false,
                NoSound = false,
                ResizableWindow = true,
                UseDirectX11 = true,
                WindowedMode = false
            };

            surface.Show(typeof(UrhoApplication.Myastro), _appOptions);

            Window.SetFlags(WindowManagerFlags.Fullscreen,WindowManagerFlags.Fullscreen);

        }
        protected override void OnResume()
        {
            UrhoSurface.OnResume();
            base.OnResume();
        }

        protected override void OnPause()
        {
            UrhoSurface.OnPause();
            base.OnPause();
        }

        public override void OnLowMemory()
        {
            UrhoSurface.OnLowMemory();
            base.OnLowMemory();
        }

        protected override void OnDestroy()
        {
            UrhoSurface.OnDestroy();
            base.OnDestroy();
        }

        public override bool DispatchKeyEvent(KeyEvent e)
        {
            if (!UrhoSurface.DispatchKeyEvent(e))
                return false;
            return base.DispatchKeyEvent(e);
        }

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            UrhoSurface.OnWindowFocusChanged(hasFocus);
            base.OnWindowFocusChanged(hasFocus);
        }
    }
}

