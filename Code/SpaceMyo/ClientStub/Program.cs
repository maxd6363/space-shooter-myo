﻿using Microsoft.AspNetCore.SignalR.Client;
using System;

namespace ClientStub
{
    class Program
    {

        private static HubConnection Hub { get; set; }

        static void Main(string[] args)
        {
            Hub = new HubConnectionBuilder().WithUrl("https://localhost:5000").Build();
            Console.WriteLine($"Connection established : {Hub.State}");


        }
    }
}
